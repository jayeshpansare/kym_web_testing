import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/transthoracic-doppler-echocardiogram.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Transthoracic doppler echocardiogram', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('Transthoracic doppler echocardiogram', false)

WebUI.verifyTextPresent('Transthoracic doppler echocardiogram is a type of echocardiogram where the probe is placed on the chest or abdomen of the subject to get various views of the heart.', 
    false)

