import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/blood-pressure.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Blood Pressure', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('BLOOD PRESSURE LEVELS', false)

WebUI.verifyTextPresent('Blood Pressure', false)

WebUI.verifyTextPresent('Blood pressure is the pressure of circulating blood on the walls of your blood vessels.', false)

WebUI.verifyTextPresent('Blood pressure is determined by how much blood your heart pumps and the amount of resistance in the arteries to the blood flow.', 
    false)

WebUI.verifyTextPresent('Blood pressure reading is determined by measuring your systolic and diastolic blood pressures.', 
    false)

WebUI.verifyTextPresent('Systolic blood pressure is the top number and measures the force your heart exerts on the walls of your arteries each time it beats.', 
    false)

WebUI.verifyTextPresent('Diastolic blood pressure is the bottom number and measures the force your heart exerts on the walls of your arteries in between beats.', 
    false)

WebUI.verifyTextPresent('Measurements and Stages', false)

WebUI.verifyTextPresent('Measurements and stages are based upon appropriately measured blood pressure techniques.', false)

WebUI.verifyTextPresent('Normal blood pressure – Systolic <120 mmHg and diastolic <80 mmHg (120/80 mmHg)', false)

WebUI.verifyTextPresent('Elevated blood pressure – Systolic 120 to 129 mmHg and diastolic <80 mmHg', false)

WebUI.verifyTextPresent('Hypertension', false)

WebUI.verifyTextPresent('Stage 1 – Systolic 130 to 139 mmHg or diastolic 80 to 89 mmHg', false)

WebUI.verifyTextPresent('Stage 2 – Systolic at least 140 mmHg or diastolic at least 90 mmHg', false)

WebUI.verifyTextPresent('If there is a disparity in category between the systolic and diastolic pressures, the higher value determines the stage', 
    false)

WebUI.verifyTextPresent('Isolated systolic hypertension is defined as blood pressure ≥130/<80 mmHg. Isolated diastolic hypertension is defined as a blood pressure <130/≥80 mmHg.', 
    false)

WebUI.verifyTextPresent('Checking the blood pressure in doctor’s office, or hospital settings', false)

WebUI.verifyTextPresent('The test may be performed by a nurse or a doctor in doctor’s office, or hospital settings.', false)

WebUI.verifyTextPresent('Blood pressure test is performed best while you\'re seated in a chair in the examining room. Rest your arms on a table at heart level, both feet flat on the floor and back supported by the chair.', 
    false)

WebUI.verifyTextPresent('Your nurse or doctor will wrap an inflatable cuff around the top part of your arm. The bottom of the inflatable cuff is just above your elbow. The cuff is attached to a dial, a device, digital display that looks similar to a thermometer. This equipment is called a sphygmomanometer.', 
    false)

WebUI.verifyTextPresent('Do not move throughout the test.', false)

WebUI.verifyTextPresent('The nurse or doctor will feel the pulse at your wrist.', false)

WebUI.verifyTextPresent('Once the nurse or doctor finds a pulse from an artery. He or she will place the stethoscope above the elbow to listen for the blood flow. Then he or she will begin inflating the cuff to momentarily stop the blood flow through the artery in your arm.', 
    false)

WebUI.verifyTextPresent('Then the nurse or doctor will open a valve on the hand pump to slowly release the air in the cuff.', 
    false)

WebUI.verifyTextPresent('Your doctor or nurse will listen to your pulse with a stethoscope to record your systolic and diastolic blood pressure.', 
    false)

WebUI.verifyTextPresent('Checking the blood pressure at home', false)

WebUI.verifyTextPresent('There are various blood pressure monitors available. Your doctor or nurse can help you choose the right device for you.', 
    false)

WebUI.verifyTextPresent('Once you have a home blood pressure meter, your doctor or nurse should check it to make sure it fits you and works correctly.', 
    false)

WebUI.verifyTextPresent('When it\'s time to check your blood pressure', false)

WebUI.verifyTextPresent('Sit in a chair with your feet flat on the ground.', false)

WebUI.verifyTextPresent('Stay calm and try to breathe normally.', false)

WebUI.verifyTextPresent('Attach the cuff to your arm. Place the cuff directly on your skin. Make sure you do not put the cuff over your clothing. The cuff should be tight enough to not slip down.', 
    false)

WebUI.verifyTextPresent('Follow the directions that come with your device. This might involve squeezing the bulb at the end of the tube to inflate the cuff (fill it with air). With some monitors, you just need to press a button to inflate the cuff.', 
    false)

WebUI.verifyTextPresent('The cuff fills with air, it will feel like someone is squeezing your arm, but it should not hurt. Then you will slowly deflate the cuff (let the air out of it), or it will deflate by itself. The screen or dial will show your blood pressure numbers.', 
    false)

WebUI.verifyTextPresent('Seek immediate medical help if:', false)

WebUI.verifyTextPresent('Systolic blood pressure is above and equal to 180 mmHg and/or diastolic pressure ≥120 mmHg.', false)

WebUI.verifyTextPresent('Systolic blood pressure lower than 90 mmHg or diastolic blood pressure lower than 60 mmHg with symptoms such as fainting, dizziness, blurred vision, nausea, lack of concentration.', 
    false)

WebUI.verifyTextPresent('Sudden drop in blood pressure due to various reasons like low or high body temperature, dehydration, bleeding or an allergic reaction.', 
    false)

WebUI.verifyTextPresent('Blood pressure taken varies widely during the day, being influenced by factors such as stress (particularly at work), smoking, caffeine intake, and exercise.', 
    false)

WebUI.verifyTextPresent('The frequency of measuring blood pressure is based on your doctor’s recommendation. Different people need to follow different schedules. Some people need to check their blood pressure twice a day, in the morning and evening.', 
    false)

WebUI.verifyTextPresent('Keep a log of your blood pressure readings and share it with your doctor.', false)


