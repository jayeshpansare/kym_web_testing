import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/heart-rate.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Heart Rate', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('HEART RATE', false)

WebUI.verifyTextPresent('Resting Heart Rate', false)

WebUI.verifyTextPresent('Resting heart rate is number of times the heart beats per minute.', false)

WebUI.verifyTextPresent('There are many factors that influence the heart rate such as age, smoking, cardiovascular disease, air temperature, emotions, medications.', 
    false)

WebUI.verifyTextPresent('Normal heart beat is steady, even rhythm. Cardiac arrhythmias are disturbances in the normal rhythm of the heartbeat. In arrhythmias, the heart may beat too rapidly or too slowly, or it may beat irregularly.', 
    false)

WebUI.verifyTextPresent('Heart rate in awake, healthy infants, children, and adults at rest', false)

WebUI.verifyTextPresent('0-3 months - 123-164', false)

WebUI.verifyTextPresent('3 to <6 months - 120-159', false)

WebUI.verifyTextPresent('6 to <9 months - 114-152', false)

WebUI.verifyTextPresent('9 to <12 months - 109-145', false)

WebUI.verifyTextPresent('12 to <18 months - 103-140', false)

WebUI.verifyTextPresent('18 to <24 months - 98-135', false)

WebUI.verifyTextPresent('2 to <3 years - 92-128', false)

WebUI.verifyTextPresent('3 to <4 years - 86-123', false)

WebUI.verifyTextPresent('4 to <6 years - 81-117', false)

WebUI.verifyTextPresent('6 to <8 years - 74-111', false)

WebUI.verifyTextPresent('8 to <12 years -67-103', false)

WebUI.verifyTextPresent('12 to <15 years -62-96', false)

WebUI.verifyTextPresent('15 to 18 years - 58-92', false)

WebUI.verifyTextPresent('Adults - 60-100', false)

WebUI.verifyTextPresent('Well trained athletes - 40-60', false)

WebUI.verifyTextPresent('How to measure the Heart Rate (pulse)', false)

WebUI.verifyTextPresent('Heart rate can be measured by placing your index and middle finger firmly and gently on the artery at either lower neck, inside your elbow or inside your wrist. When taking your pulse:', 
    false)

WebUI.verifyTextPresent('Have a watch so you will know how many beats you count in a minute.', false)

WebUI.verifyTextPresent('Count the beats in a 60-second period. Or, you can count the beats in a 15-second period and multiply that number by four.', 
    false)

WebUI.verifyTextPresent('Take your pulse at least twice to get an accurate reading.', false)

WebUI.verifyTextPresent('If you are having trouble feeling your pulse, ask another person to count for you.', false)

WebUI.verifyTextPresent('Maximum Heart Rate', false)

WebUI.verifyTextPresent('Maximum heart rate is the number used to calculate your target heart rate zone.', false)

WebUI.verifyTextPresent('Target Heart Rate', false)

WebUI.verifyTextPresent('Target heart rate is expressed as a percentage. It is a range that reflects how fast the heart should be beating when you exercise.', 
    false)

WebUI.verifyTextPresent('To calculate your target heart rate, subtract your age from 220 to reveal your maximum heart rate. A target heart rate is a percentage of the maximum heart rate.', 
    false)

WebUI.verifyTextPresent('Exercising at your target heart rate can boost the cardiovascular benefit. During exercise 50 to 85% of your maximum heart rate is recommended.', 
    false)

WebUI.verifyTextPresent('Consult with your doctor if you are an adult and:', false)

WebUI.verifyTextPresent('If your resting heart rate is consistently above 100 beats a minute.', false)

WebUI.verifyTextPresent('If you\'re not a trained athlete and your resting heart rate is below 60 beats per minute.', false)

WebUI.verifyTextPresent('If you\'re not a trained athlete and your resting heart rate is below 60 beats per minute and you have other signs or symptoms, such as dizziness, fainting, or shortness of breath.', 
    false)



