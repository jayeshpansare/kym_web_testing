import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/temperature.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Body Temperature', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('BODY TEMPERATURE', false)

WebUI.verifyTextPresent('Body temperature is the average temperature of the human body which varies by person, age, activity, and time of day. ', 
    false)

WebUI.verifyTextPresent('Normally the body is able to maintain a fairly steady temperature because the thermoregulatory center in the brain balances the excess heat production, with heat dissipation from the lungs and skin. Body temperature normally changes throughout the day, but there is a narrow daily variation of the temperature. ', 
    false)

WebUI.verifyTextPresent('However, when faced with environmental extremes, humans cannot maintain this narrow daily variation.', 
    false)

WebUI.verifyTextPresent('Things to keep in mind while taking your temperature', false)

WebUI.verifyTextPresent('Most accurate temperature readings are possible if you follow the instructions that come with the thermometer.', 
    false)

WebUI.verifyTextPresent('Avoid hot or cold liquids within 30 minutes of taking your temperature.', false)

WebUI.verifyTextPresent('keep your mouth closed around the thermometer when taking your temperature orally.', false)

WebUI.verifyTextPresent('keep the thermometer in place long enough.', false)

WebUI.verifyTextPresent('Methods for taking temperature', false)

WebUI.verifyTextPresent('Ways to take (measure) a temperature are:', false)

WebUI.verifyTextPresent('Orally - Turn on the thermometer. The Oral temperature is taken by placing the tip of the thermometer under the tongue toward the back of the mouth. Keep your lips closed. Remove the thermometer when it signals that it\'s done and read the number.', 
    false)

WebUI.verifyTextPresent('Rectally - Lubricate the tip of the thermometer with petroleum jelly after turning it on. Lay the person on his or her back, lift his or her thighs, and insert the lubricated thermometer 1/2 to 1 inch (1.3 to 2.5 centimeters) into the rectum. Alternatively, for a child, you can place the child on his or her belly on your lap or another firm surface. Put your hand against his or her lower back to hold the child in place. Do not force the thermometer past any resistance. Hold it in place until the thermometer signals that it\'s done. Remove the thermometer and read the number.', 
    false)

WebUI.verifyTextPresent('Axillary - In this method, the thermometer is placed under the armpit. After turning on the thermometer, place the thermometer under the armpit making sure it touches skin and not clothing. Keep the thermometer tightly in place until the thermometer signals that it\'s done. Remove it and read the number.', 
    false)

WebUI.verifyTextPresent('Aurally - In this method of checking the temperature, the thermometer is placed in the ear. Gently place the thermometer in his or her ear after turning it on. Ensure that you insert the thermometer the proper distance into the ear canal. Keep the thermometer in place until the thermometer signals that it\'s done. Remove it and read the number.', 
    false)

WebUI.verifyTextPresent('Temporally - Gently sweep the thermometer across the forehead to the side of the eye, after turning it on. Remove the thermometer and read the number.', 
    false)

WebUI.verifyTextPresent('Temperature readings', false)

WebUI.verifyTextPresent('Normal body temperature is 98.6°F (37°C).', false)

WebUI.verifyTextPresent('Temperature greater than 100.4°F (38°C) is considered a fever. Infection is the most common cause of fever. Other noninfectious causes of fever are autoimmune and autoinflammatory diseases.', 
    false)

WebUI.verifyTextPresent('Temperature falling below 95 F (35 C) is when hypothermia occurs. Hypothermia is often caused by exposure to cold weather or immersion in cold water. Hypothermia is a medical emergency and if left untreated can eventually lead to complete failure of your heart and respiratory system and eventually to death.', 
    false)

WebUI.verifyTextPresent('Extraordinarily high fever (>41.5°C), is termed as hyperpyrexia and is seen most commonly in central nervous system hemorrhages.', 
    false)

WebUI.verifyTextPresent('Hyperthermia is a form of elevated body temperature in few instances like heat stroke, use of certain drugs that interfere with normal thermoregulation, or metabolic conditions like hyperthyroidism. Hyperthermia is a medical emergency as it can be rapidly fatal.', 
    false)

WebUI.verifyTextPresent('Seek immediate medical help for a fever', false)

WebUI.verifyTextPresent('If the fever lasts for more than 4-5 days.', false)

WebUI.verifyTextPresent('Small babies under the age of 3 months old.', false)

WebUI.verifyTextPresent('If you are a senior citizen with a fever.', false)

WebUI.verifyTextPresent('If you have cancer and undergoing chemotherapy.', false)

WebUI.verifyTextPresent('If you have certain medical problems which make you immunocompromised (i.e. sickle cell disease, diabetes, HIV, etc.).', 
    false)

WebUI.verifyTextPresent('If you are taking steroids long-term.', false)

WebUI.verifyTextPresent('If the fever is really high (104° Fahrenheit).', false)

WebUI.verifyTextPresent('If the person with the fever looks really ill or has trouble breathing, a change in their behavior, headache or neck stiffness.', 
    false)

