import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/generalized-anxiety-disorder.html'

WebUI.navigateToUrl(pageURL)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Generalized Anxiety Disorder', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('GENERALIZED ANXIETY DISORDER', false)

WebUI.verifyTextPresent('Overview of Generalized Anxiety Disorder', false)

WebUI.verifyTextPresent('A disorder that causes significant distress or impairment, and is characterized by excessive and persistent worrying which is hard to control.', 
    false)

WebUI.verifyTextPresent('Genetic factors appear to predispose people to the development of generalized anxiety disorder.', 
    false)

WebUI.verifyTextPresent('Adversity and undesirable life events can exacerbate symptoms of generalized anxiety disorder.', 
    false)

WebUI.verifyTextPresent('Symptoms of Generalized Anxiety Disorder', false)

WebUI.verifyTextPresent('Persistent worrying or anxiety that is out of proportion.', false)

WebUI.verifyTextPresent('Inability to set aside or let go of a worry.', false)

WebUI.verifyTextPresent('Overthinking plans and solutions.', false)

WebUI.verifyTextPresent('Poor sleep.', false)

WebUI.verifyTextPresent('Fatigue', false)

WebUI.verifyTextPresent('Difficulty relaxing.', false)

WebUI.verifyTextPresent('Headaches and pain in the neck, shoulders, and back.', false)

WebUI.verifyTextPresent('Associated conditions and complications', false)

WebUI.verifyTextPresent('Generalized anxiety disorder is associated with poor cardiovascular health and coronary heart disease. Excessive worrying is associated with an increase in heart rate and blood pressure.', 
    false)

WebUI.verifyTextPresent('Increased risk of depression.', false)

WebUI.verifyTextPresent('DSM-5 diagnostic criteria for generalized anxiety disorder require the presence of', false)

WebUI.verifyTextPresent('A. Excessive anxiety and worry (apprehensive expectation), occurring more days than not for at least 6 months, about a number of events or activities (such as work or school performance).', 
    false)

WebUI.verifyTextPresent('B. The individual finds it difficult to control the worry.', false)

WebUI.verifyTextPresent('C. The anxiety and worry are associated with three (or more) of the following six symptoms (with at least some symptoms having been present for more days than not for the past 6 months):', 
    false)

WebUI.verifyTextPresent('Note: Only one item required in children.', false)

WebUI.verifyTextPresent('Restlessness, feeling keyed up or on edge.', false)

WebUI.verifyTextPresent('Being easily fatigued.', false)

WebUI.verifyTextPresent('Difficulty concentrating or mind going blank.', false)

WebUI.verifyTextPresent('Irritability', false)

WebUI.verifyTextPresent('Muscle tension.', false)

WebUI.verifyTextPresent('Sleep disturbance (difficulty falling or staying asleep, or restless, unsatisfying sleep).', false)

WebUI.verifyTextPresent('D. The anxiety, worry, or physical symptoms cause clinically significant distress or impairment in social, occupational, or other important areas of functioning.', 
    false)

WebUI.verifyTextPresent('E. The disturbance is not attributable to the physiological effects of a substance (e.g., a drug of abuse, a medication) or another medical condition (e.g., hyperthyroidism).', 
    false)

WebUI.verifyTextPresent('F. The disturbance is not better explained by another medical disorder (e.g., anxiety or worry about having panic attacks in panic disorder, negative evaluation in social anxiety disorder [social phobia], contamination or other obsessions in obsessive-compulsive disorder, separation from attachment figures in separation anxiety disorder, reminders of traumatic events in posttraumatic stress disorder, gaining weight in anorexia nervosa, physical complaints in somatic symptom disorder, perceived appearance flaws in body dysmorphic disorder, having a serious illness in illness anxiety disorder, or the content of delusional beliefs in schizophrenia or delusional disorder).', 
    false)

WebUI.verifyTextPresent('Management of Generalized Anxiety Disorder', false)

WebUI.verifyTextPresent('Lifestyle management', false)

WebUI.verifyTextPresent('Use relaxation techniques such as meditation, yoga, visualization techniques to ease anxiety.', 
    false)

WebUI.verifyTextPresent('Eat healthily and be physically active, as recommended by your doctor. Exercise improves your mood and helps you to stay healthy.', 
    false)

WebUI.verifyTextPresent('Make sure you are well-rested and getting enough sleep. Talk to your doctors, if you aren\'t sleeping well.', 
    false)

WebUI.verifyTextPresent('Alcohol and recreational drugs can worsen anxiety. Avoid alcohol and recreational drugs.', false)

WebUI.verifyTextPresent('Quit smoking, as nicotine can worsen anxiety.', false)

WebUI.verifyTextPresent('Quit drinking coffee, as caffeine can worsen anxiety.', false)

WebUI.verifyTextPresent('When you feel anxious, delve into a hobby to refocus your mind away from your worries.', false)

WebUI.verifyTextPresent('Social interaction and participating in enjoyable activities can lessen your worries.', false)

WebUI.verifyTextPresent('Psychotherapy', false)

WebUI.verifyTextPresent('Cognitive behavioral therapy - Cognitive behavioral therapy is the most effective form of psychotherapy which involves working with a therapist to reduce your anxiety symptoms.', 
    false)

WebUI.verifyTextPresent('Medications', false)

WebUI.verifyTextPresent('Antidepressants - Some of the antidepressants used to treat generalized anxiety disorder are escitalopram, duloxetine, venlafaxine, and paroxetine.', 
    false)

WebUI.verifyTextPresent('Buspirone', false)

WebUI.verifyTextPresent('In some circumstances, your doctor may prescribe benzodiazepines.', false)

WebUI.verifyTextPresent('GAD-7 anxiety scale in adults', false)

WebUI.verifyTextPresent('GAD-7 is a screening tool and symptom severity measure for generalized anxiety disorder is available to everyone. Talk to your doctor about how often to use the tool and share the questionnaire tool with your doctor.', 
    false)


