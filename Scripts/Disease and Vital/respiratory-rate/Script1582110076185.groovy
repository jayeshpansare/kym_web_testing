import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/respiratory-rate.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Respiratory Rate', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('RESPIRATORY RATE', false)

WebUI.verifyTextPresent('Respiratory rate is the rate at which breathing occurs and is usually measured in breaths per minute.', 
    false)

WebUI.verifyTextPresent('Respiratory rate is controlled by the respiratory center in the brain. The respiratory center controls the rate and depth of respiratory movements of the diaphragm and other respiratory muscles.', 
    false)

WebUI.verifyTextPresent('Respiratory rate may increase with illness or other medical conditions.', false)

WebUI.verifyTextPresent('How to measure the respiratory rate', false)

WebUI.verifyTextPresent('The respiratory rate is measured when the person is at rest by counting how many times the chest rises for one minute.', 
    false)

WebUI.verifyTextPresent('Normal range', false)

WebUI.verifyTextPresent('The average resting respiratory rate in healthy newborn, children and adults is:', false)

WebUI.verifyTextPresent('0 to 3 months - 34-57', false)

WebUI.verifyTextPresent('3 to <6 months - 33-55', false)

WebUI.verifyTextPresent('6 to <9 months - 31-52', false)

WebUI.verifyTextPresent('9 to <12 months - 30-50', false)

WebUI.verifyTextPresent('12 to <18 months - 28-46', false)

WebUI.verifyTextPresent('18 to <24 months - 25-40', false)

WebUI.verifyTextPresent('2 to <3 years - 22-34', false)

WebUI.verifyTextPresent('3 to <4 years - 21-29', false)

WebUI.verifyTextPresent('4 to <6 years - 20-27', false)

WebUI.verifyTextPresent('6 to <8 years - 18-24', false)

WebUI.verifyTextPresent('8 to <12 years - 16-22', false)

WebUI.verifyTextPresent('12 to <15 years - 15-21', false)

WebUI.verifyTextPresent('15 to 18 years - 13-19', false)

WebUI.verifyTextPresent('Adults - 12-20', false)

WebUI.verifyTextPresent('Abnormal respiratory rates:', false)

WebUI.verifyTextPresent('Apnea - Cessation of breathing is called apnea. There is no movement of the muscles of inhalation. Voluntarily doing this is called holding one’s breath. Untrained people cannot sustain voluntary apnea for more than one or two minutes.The causes of apnea are drug-induced, strangulation, sleep apnea.', 
    false)

WebUI.verifyTextPresent('Dyspnea - Dyspnea is also called shortness of breath. Shortness of breath is the feeling of not breathing well enough. Dyspnea can occur in heavy exertion normally, or in conditions such as asthma, pneumonia, chronic obstructive pulmonary disease, congestive heart failure, interstitial lung disease, anemia, acute chest syndrome, panic disorder, anxiety.', 
    false)

WebUI.verifyTextPresent('Hyperpnea - Hyperpnea is increased rate and depth of breathing. Hyperpnea may occur due to physiologic response such as seen during or after exercise, or it may be due to a pathologic response as seen when sepsis is severe.', 
    false)

WebUI.verifyTextPresent('Tachypnea - Tachypnea is abnormally rapid breathing. In adults at rest, tachypnea is indicated by a rate greater than 20 breaths per minute. The causes of tachypnea are exercise, sepsis, diabetic ketoacidosis, metabolic acidosis, asthma, chronic obstructive lung disease, pulmonary embolism, congestive heart failure, anxiety, allergic reaction.', 
    false)

WebUI.verifyTextPresent('Orthopnea - Orthopnea is dyspnea that occurs while lying flat. Orthopnea is often a symptom of heart failure and/or pulmonary edema. It can also occur in asthma, chronic bronchitis, sleep apnea, panic disorder, polycystic liver disease.', 
    false)

WebUI.verifyTextPresent('Paroxysmal nocturnal dyspnea - Paroxysmal nocturnal dyspnea refers to severe dyspnea or shortness of breath that occurs at night and usually awakens the person from sleep.', 
    false)

WebUI.verifyTextPresent('Monitoring of respiratory rate in critical care areas', false)

WebUI.verifyTextPresent('In critical care areas, respiratory rates are monitored by capnography monitors or impedance pneumography. These devices are used in people who are sedated and intubated.', 
    false)


