import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/weight.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Weight', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('BODYWEIGHT', false)

WebUI.verifyTextPresent('Weight of a person, measured in units such as kilograms, pounds, or tons.', false)

WebUI.verifyTextPresent('The ranges of weight are “underweight”, “normal”, “overweight”, and “obese”.', false)

WebUI.verifyTextPresent('Obese and overweight - weight is greater than it should be for your health.', false)

WebUI.verifyTextPresent('Underweight - Weight is lower than it should be for your health.', false)

WebUI.verifyTextPresent('Healthy body weight depends on your sex and height. For children, it also depends on your age.', 
    false)

WebUI.verifyTextPresent('Body Mass Index', false)

WebUI.verifyTextPresent('BMI is defined as the weight in kilograms divided by height in meters squared. [BMI = body weight (in kg) ÷ height (in meters) squared]', 
    false)

WebUI.verifyTextPresent('BMI provides a better estimate of total body fat compared with bodyweight alone.', false)

WebUI.verifyTextPresent('BMI Interpretation', false)

WebUI.verifyTextPresent('BMI <18.5: Below normal weight.', false)

WebUI.verifyTextPresent('BMI ≥18.5 and <25: Normal weight.', false)

WebUI.verifyTextPresent('BMI ≥25 and <30: Overweight.', false)

WebUI.verifyTextPresent('BMI ≥30 and <35: Class I Obesity', false)

WebUI.verifyTextPresent('BMI ≥35 and <40: Class II Obesity', false)

WebUI.verifyTextPresent('BMI ≥40: Class III Obesity', false)

WebUI.verifyTextPresent('Obesity-related health risk', false)

WebUI.verifyTextPresent('Obesity is a risk factor for several diseases including established coronary heart disease (CHD), other atherosclerotic diseases, type 2 diabetes mellitus, sleep apnea, non-alcoholic fatty liver, polycystic ovary syndrome, hypertension.', 
    false)

WebUI.verifyTextPresent('Underweight related health risk', false)

WebUI.verifyTextPresent('Underweight related health risks include malnutrition, anemia, osteoporosis, fertility issues, growth and development issues in children and teenagers.', 
    false)

WebUI.verifyTextPresent('A sudden, unexpected change in weight can be a sign of a medical problem.', false)

WebUI.verifyTextPresent('Causes of sudden weight loss', false)

WebUI.verifyTextPresent('Cancer', false)

WebUI.verifyTextPresent('Thyroid problems', false)

WebUI.verifyTextPresent('Digestive diseases', false)

WebUI.verifyTextPresent('Infectious diseases', false)

WebUI.verifyTextPresent('Certain medicines', false)

WebUI.verifyTextPresent('Causes of sudden weight gain', false)

WebUI.verifyTextPresent('Heart failure.', false)

WebUI.verifyTextPresent('Kidney disease.', false)

WebUI.verifyTextPresent('Thyroid problems.', false)

WebUI.verifyTextPresent('Waist circumference', false)

WebUI.verifyTextPresent('People with abdominal obesity are at increased risk for heart disease, diabetes, dyslipidemia, hypertension, and nonalcoholic fatty liver disease.', 
    false)

WebUI.verifyTextPresent('Waist circumference provides risk information that is not accounted for by BMI.', false)

WebUI.verifyTextPresent('Doctors may use waist circumference and BMI for identifying adults at increased risk for morbidity and mortality.', 
    false)

WebUI.verifyTextPresent('Talk to your doctor about the ideal body weight for you and follow the diet and physical activity recommended by your doctor.', 
    false)

WebUI.verifyTextPresent('You can use the table below, to see which category you fit into – healthy weight, overweight, or obese:', 
    false)

