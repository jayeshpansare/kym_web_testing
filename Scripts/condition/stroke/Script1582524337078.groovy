import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/stroke.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Stroke', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('STROKE', false)

WebUI.verifyTextPresent('Overview of Stroke', false)

WebUI.verifyTextPresent('Stroke is a condition that occurs when the blood supply to the brain is interrupted or reduced. Because of this the brain is deprived of oxygen and nutrients.', 
    false)

WebUI.verifyTextPresent('Symptoms of Stroke', false)

WebUI.verifyTextPresent('Stroke is a medical emergency and needs immediate medical attention if there are', false)

WebUI.verifyTextPresent('Face drooping on one side of the face when you smile.', false)

WebUI.verifyTextPresent('One arm drift downwards or if one arm is unable to rise up', false)

WebUI.verifyTextPresent('Speech is slurred or strange', false)

WebUI.verifyTextPresent('Other signs and symptoms are', false)

WebUI.verifyTextPresent('Confusion', false)

WebUI.verifyTextPresent('Sudden numbness, weakness or paralysis in your face, arm or leg. This often happens just on one side of your body.', 
    false)

WebUI.verifyTextPresent('Blurred or blackened vision in eye/eyes, or you may see double.', false)

WebUI.verifyTextPresent('Trouble walking.', false)

WebUI.verifyTextPresent('Sudden severe headache, may be associated with vomiting.', false)

WebUI.verifyTextPresent('Causes and different types of Stroke', false)

WebUI.verifyTextPresent('Ischemic stroke - The most common type of stroke where in the arteries that supply blood to the brain become narrowed or blocked. The ischemic stroke occurs when a blood clot forms in one of the arteries that supply blood to the brain, or blood clot that has formed elsewhere is swept through the bloodstream and gets lodged in one of the arteries that supply the brain.', 
    false)

WebUI.verifyTextPresent('Hemorrhagic stroke - Stroke that occurs when a blood vessel in the brain ruptures or leaks. There are two kinds of hemorrhagic stroke - Intracerebral hemorrhage and subarachnoid hemorrhage.', 
    false)

WebUI.verifyTextPresent('Transient ischemic stroke - Sudden onset of a focal neurologic symptoms and/or signs lasting for few minutes.There is temporary decrease of blood supply to the part of the brain. Even a few moments of neurologic symptoms and/or signs needs immediate medical attention.', 
    false)

WebUI.verifyTextPresent('Risk factors', false)

WebUI.verifyTextPresent('Heart disease, hypertension, smoking, diabetes, elevated cholesterol levels in the blood, use of amphetamines; cocaine, bleeding disorders, overtreatment with blood thinners.', 
    false)

WebUI.verifyTextPresent('Complications of Stroke', false)

WebUI.verifyTextPresent('Falls', false)

WebUI.verifyTextPresent('Pressure sores', false)

WebUI.verifyTextPresent('Depression', false)

WebUI.verifyTextPresent('Urinary tract infection', false)

WebUI.verifyTextPresent('Deep vein thrombosis - Blood clot forms in one or more veins of the leg with symptoms of leg pain, swelling , and tenderness.', 
    false)

WebUI.verifyTextPresent('Pulmonary embolism - A condition in which there is a blockage of pulmonary artery which supplies blood to the lungs. Symptoms of pulmonary embolism includes shortness of breath, chest pain and cough. Seek immediate medical help if you experience any of these symptoms.', 
    false)

WebUI.verifyTextPresent('Shoulder pain', false)

WebUI.verifyTextPresent('Chest infection', false)

WebUI.verifyTextPresent('Pressure sores', false)

WebUI.verifyTextPresent('Diagnosis of Stroke', false)

WebUI.verifyTextPresent('The type of stroke and the areas of the brain affected are confirmed by physical examination, blood tests, computerized tomography scan, MRI, carotid ultrasound, cerebral angiogram, echocardiogram.', 
    false)

WebUI.verifyTextPresent('Management of Stroke', false)

WebUI.verifyTextPresent('Lifestyle management', false)

WebUI.verifyTextPresent('Follow the diet and exercise recommended for underlying conditions such as hypertension, diabetes mellitus as these are risk factors for stroke. These lifestyle management also helps you keep your blood pressure and blood glucose within the recommended goals by the doctor.', 
    false)

WebUI.verifyTextPresent('Trans fat (hydrogenated fats) are atherogenic, while mono- and polyunsaturated fats (particularly omega-3 fatty acids) - eg, those found in fish, olive oil, nuts are protective.', 
    false)

WebUI.verifyTextPresent('Smoking cessation and avoidance of environmental tobacco smoke.', false)

WebUI.verifyTextPresent('Physical activity and exercise as recommended by your doctor.', false)

WebUI.verifyTextPresent('Moderate alcohol consumption - one or two drinks a day is recommended.', false)

WebUI.verifyTextPresent('Medical management', false)

WebUI.verifyTextPresent('Ischemic Stroke', false)

WebUI.verifyTextPresent('Tissue plasminogen activator - alteplase is ideally given within three hours of symptoms of stroke and is a gold standard treatment for ischemic stroke.', 
    false)

WebUI.verifyTextPresent('Emergency endovascular procedures.', false)

WebUI.verifyTextPresent('Carotid endarterectomy - plaque within the carotid arteries are removed.', false)

WebUI.verifyTextPresent('Angioplasty and stents.', false)

WebUI.verifyTextPresent('Hemorrhagic stroke', false)

WebUI.verifyTextPresent('Focuses on controlling your bleeding and pressure on the brain.', false)

