import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/epilepsy.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Epilepsy', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('EPILEPSY', false)

WebUI.verifyTextPresent('Overview of Epilepsy', false)

WebUI.verifyTextPresent('Epilepsy is a neurological disorder in which the brain activity is disturbed causing seizures; unusual sensations, behaviours; and sometimes loss of awareness.', 
    false)

WebUI.verifyTextPresent('Types of seizures', false)

WebUI.verifyTextPresent('Generalized epilepsy - Seizure is characterized by the onset of electrical activity that involves whole or a larger portion of the brain. The subtypes of generalized seizures are generalized tonic-clonic seizures, absence seizures, clonic seizures, myoclonic seizures, tonic seizures, atonic seizures.', 
    false)

WebUI.verifyTextPresent('Focal epilepsy - The onset of electrical activity in the brain that involves local region. Focal seizures are further classified as focal seizures with retained awareness, or focal seizures with impaired awareness, according to whether there is impairment of consciousness or not during the event.', 
    false)

WebUI.verifyTextPresent('Symptoms of Epilepsy depend on the type of seizures', false)

WebUI.verifyTextPresent('In focal seizures with retained awareness the symptoms are - change in emotions; or change in the way things smell, taste, look, feel, or sound; involuntary jerking of a body part, such as an arm or leg; dizziness, tingling, and flashing lights.', 
    false)

WebUI.verifyTextPresent('In focal seizures with impaired awareness the symptoms include - Change or loss of consciousness; stare into space and remain motionless; or engage in repetitive behaviors, such as facial grimacing, chewing, gesturing, lip smacking, or repeating words or phrases.', 
    false)

WebUI.verifyTextPresent('Absence seizures, also called petit mal seizures, most often occur in children and is characterized by staring into space, or subtle body movements such as eye blinking or lip smacking. Absence seizures can cause a brief loss of awareness and occur in clusters.', 
    false)

WebUI.verifyTextPresent('Tonic seizures affect the muscles in your arms and legs, back and may cause you to fall to the ground. There is stiffening of the muscles.', 
    false)

WebUI.verifyTextPresent('Atonic seizures, also called drop seizures may cause you to suddenly collapse or fall down because of loss of muscle control.', 
    false)

WebUI.verifyTextPresent('There is sudden brief jerks or twitches of your arms and legs, in myoclonic seizures.', false)

WebUI.verifyTextPresent('Grand mal seizures, also called tonic-clonic seizures can cause body stiffening and shaking, an abrupt loss of consciousness, and sometimes loss of bladder control or biting your tongue.', 
    false)

WebUI.verifyTextPresent('Causes of Epilepsy', false)

WebUI.verifyTextPresent('The cause of the seizure vary according to both setting and type of seizure. Older adults with a first seizure are more likely to have a cause of seizure, compared to children and young adults.', 
    false)

WebUI.verifyTextPresent('The causes of seizures could be:', false)

WebUI.verifyTextPresent('a combination of genetic and environmental factors.', false)

WebUI.verifyTextPresent('Trauma to the head.', false)

WebUI.verifyTextPresent('Brain tumors or stroke.', false)

WebUI.verifyTextPresent('Meningitis (inflammation of the membranes that cover the brain and spinal cord).', false)

WebUI.verifyTextPresent('AIDS and viral encephalitis (inflammation of the brain).', false)

WebUI.verifyTextPresent('Epilepsy may be sometimes associated with developmental disorders such as autism, neurofibromatosis (a condition that causes tumors in brain, nerves, and spinal cord).', 
    false)

WebUI.verifyTextPresent('Prenatal injury to the baby due to infection in the mother, poor nutrition, oxygen deficiencies.', 
    false)

WebUI.verifyTextPresent('Complications and comorbidities of Epilepsy', false)

WebUI.verifyTextPresent('Depression and anxiety - The Patient Health Questionnaire-9, the generalized anxiety disorder seven-item (GAD-7) scale, and Neurological Disorders Depression Inventory for Epilepsy are screening tools for depression. These are publically available and these tools have been validated in adults with epilepsy.', 
    false)

WebUI.verifyTextPresent('Psychotic disorders - Schizophrenia is most often a comorbid primary disorder.', false)

WebUI.verifyTextPresent('Cognitive impairment', false)

WebUI.verifyTextPresent('Sleep disorders - Obstructive sleep apnea, insomnia are common in patients with epilepsy', false)

WebUI.verifyTextPresent('Risk of seizure-related personal injury, such as bone fractures, falls, drowning, and other accident.', 
    false)

WebUI.verifyTextPresent('Diagnostic of Epilepsy', false)

WebUI.verifyTextPresent('Diagnosis of seizures begins with detailed history to rule out alternative diagnoses. Complete description of the event is obtained from the patient and witnesses.', 
    false)

WebUI.verifyTextPresent('Laboratory evaluations of a first seizure include electrolytes - calcium level, magnesium level, glucose, complete blood count, liver function tests, renal function tests, urinalysis, and toxicology screens.', 
    false)

WebUI.verifyTextPresent('Electroencephalogram, computerized tomography, positron emission tomography, neuropsychological tests may be done to detect brain abnormalities.', 
    false)

WebUI.verifyTextPresent('Combination of analysis techniques such as statistical parametric mapping, curry analysis and magnetoencephalography is done to help pinpoint where in the brain seizures start.', 
    false)

WebUI.verifyTextPresent('Management of Epilepsy', false)

WebUI.verifyTextPresent('Lifestyle management', false)

WebUI.verifyTextPresent('Do not stop taking an antiseizure drug and make sure your prescription does not run out or expire. Nonadherence to antiseizure drug may increase risk of hospitalization and injury.', 
    false)

WebUI.verifyTextPresent('Do not start taking any other medication without first contacting your doctor because some of the medications might affect blood concentrations of the antiseizure drugs.', 
    false)

WebUI.verifyTextPresent('You or a family member should maintain a seizure diary. Note down the time at which any symptoms occur. This diary may be used to track your response to drug therapy, including possible side effects. Seizure triggers should be indicated.', 
    false)

WebUI.verifyTextPresent('Do not drink alcohol excessively. Heavier alcohol intake (three or more drinks per day) increases the risk of seizures.', 
    false)

WebUI.verifyTextPresent('Lack of sleep, stress can trigger seizures. Make sure you get adequate rest every night.', false)

WebUI.verifyTextPresent('Antiseizure drugs can limit the effectiveness of oral contraception; alternative forms of birth control should be considered in women taking these antiseizure drugs.', 
    false)

WebUI.verifyTextPresent('Wear a medical alert bracelet all the time.', false)

WebUI.verifyTextPresent('Follow a healthy diet and exercise as recommended by your doctor.', false)

WebUI.verifyTextPresent('Call your doctor immediately if you notice new or increased feelings of depression, suicidal thoughts, or unusual changes in your mood or behavior.', 
    false)

WebUI.verifyTextPresent('Educate people around you about epilepsy and also correct way to handle a seizure in case they are with you when you have one. Correct ways to handle a seizure would be to:', 
    false)

WebUI.verifyTextPresent('Carefully roll the person onto one side.', false)

WebUI.verifyTextPresent('Place something soft under his or her head.', false)

WebUI.verifyTextPresent('Loosen any tight clothing around the neck.', false)

WebUI.verifyTextPresent('Don\'t try to restrain someone having a seizure.', false)

WebUI.verifyTextPresent('Don\'t try to put your fingers or anything else in the person\'s mouth.', false)

WebUI.verifyTextPresent('If the person is moving, clear away dangerous objects.', false)

WebUI.verifyTextPresent('Stay with the person until medical personnel arrive.', false)

WebUI.verifyTextPresent('Observe the person closely so that you can provide details on what happened.', false)

WebUI.verifyTextPresent('Time the seizures.', false)

WebUI.verifyTextPresent('Stay calm during the seizures.', false)

WebUI.verifyTextPresent('Medical management', false)

WebUI.verifyTextPresent('Anti-seizure medications - Carbamazepine, eslicarbazepine, lacosamide, lamotrigine, oxcarbazepine, phenytoin, rufinamide, zonisamide, ethosuximide, clobazam, phenobarbital, tiagabine, vigabatrin, perampanel, felbamate, topiramate, valproate, brivaracetam, gabapentin, levetiracetam, pregabalin.', 
    false)

WebUI.verifyTextPresent('Surgery may be done when medications fail to provide adequate control over seizures.', false)

WebUI.verifyTextPresent('Vagal nerve stimulation may an alternative for treating epilepsy.', false)

