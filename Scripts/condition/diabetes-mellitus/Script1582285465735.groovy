import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/diabetes-mellitus.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Diabetes Mellitus', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('DIABETES MELLITUS (TYPE 2)', false)

WebUI.verifyTextPresent('Overview of Diabetes Mellitus (TYPE 2)', false)

WebUI.verifyTextPresent('Diabetes mellitus type 2 is a chronic condition that affects the metabolism of sugar (glucose) which leads to high levels of sugar in the body.', 
    false)

WebUI.verifyTextPresent('Insulin is a hormone that regulates the movement of sugar into your cells. In diabetes type 2 your body does not make or use insulin well which results in high blood sugar levels.', 
    false)

WebUI.verifyTextPresent('The exact cause of type 2 diabetes mellitus is unknown. The contributing factors for the development of type 2 diabetes mellitus are genetics, overweight and inactive lifestyle.', 
    false)

WebUI.verifyTextPresent('Symptoms of Diabetes Mellitus (TYPE 2)', false)

WebUI.verifyTextPresent('Increased thirst, increased hunger, frequent urination, blurred vision, fatigue, frequent infections.', 
    false)

WebUI.verifyTextPresent('Risk factors for Diabetes Mellitus (TYPE 2)', false)

WebUI.verifyTextPresent('Overweight, inactivity, family history of type 2 diabetes mellitus in parent or sibling, prediabetes (Blood sugar is higher than normal but not high enough to be classified as diabetes), gestational diabetes (High blood sugar that develops during pregnancy)', 
    false)

WebUI.verifyTextPresent('Complications of Diabetes Mellitus (TYPE 2)', false)

WebUI.verifyTextPresent('Diabetic retinopathy (Retina is damaged by high blood sugar levels over time. The retina is light-sensitive tissue at the back of your eye).', 
    false)

WebUI.verifyTextPresent('Diabetic nephropathy (Damage to the kidneys).', false)

WebUI.verifyTextPresent('Diabetic neuropathy (Damage to the covering on the nerves, or blood vessels that bring oxygen to the nerves).', 
    false)

WebUI.verifyTextPresent('Atherosclerosis (a disease in which plaque builds up inside your arteries).', false)

WebUI.verifyTextPresent('Atherosclerosis can cause coronary artery disease, stroke, peripheral artery disease.', false)

WebUI.verifyTextPresent('Foot ulcers.', false)

WebUI.verifyTextPresent('Gum disease.', false)

WebUI.verifyTextPresent('Watch out for symptoms and signs based on the complication and progression of diabetes mellitus', 
    false)

WebUI.verifyTextPresent('Tingling, numbness, burning, or shooting pain in your feet Callus/calluses on the feet, loss of nails, itching, or changes in the texture of skin.', 
    false)

WebUI.verifyTextPresent('Imbalance and unsteadiness in gait with increased likelihood of falls.', false)

WebUI.verifyTextPresent('Lightheadedness, weakness, faintness, dizziness when standing from lying or sitting position.', 
    false)

WebUI.verifyTextPresent('Constipation, diarrhea, or fecal incontinence (inability to control bowel movements).', false)

WebUI.verifyTextPresent('Feel full after eating a small amount of food.', false)

WebUI.verifyTextPresent('Sexual dysfunction in men- erectile dysfunction, decreased libido, abnormal ejaculation. Women - decreased sexual desire or pain during intercourse.', 
    false)

WebUI.verifyTextPresent('Urinary frequency and urgency, or urinary incontinence (inability to control urination).', false)

WebUI.verifyTextPresent('Recurrent urinary tract infection.', false)

WebUI.verifyTextPresent('Decreased visual acuity.', false)

WebUI.verifyTextPresent('Curtain falling or floaters in your eyes.', false)

WebUI.verifyTextPresent('Leg pain during exercise and goes away with rest.', false)

WebUI.verifyTextPresent('Diagnostic criteria for Diabetes Mellitus (TYPE 2)', false)

WebUI.verifyTextPresent('Classic symptoms of type 2 diabetes (increased thirst, frequent urination, weight loss, blurry vision), and a random blood glucose value of 200 mg/dL (11.1 mmol/L) or higher.', 
    false)

WebUI.verifyTextPresent('An asymptomatic individual with Fasting plasma glucose (FPG) values ≥126 mg/dL (7.0 mmol/L) or two-hour plasma glucose values of ≥200 mg/dL (11.1 mmol/L) during an oral glucose tolerance test (OGTT) or A1C values ≥6.5 percent (48 mmol/mol). In the absence of unequivocal symptomatic hyperglycemia(high blood sugar levels), the diagnosis of diabetes must be confirmed on a subsequent day by repeat measurement, repeating the same test for confirmation.', 
    false)

WebUI.verifyTextPresent('Management of Diabetes Mellitus (TYPE 2)', false)

WebUI.verifyTextPresent('Lifestyle management', false)

WebUI.verifyTextPresent('Diet recommended for Diabetes Mellitus - People with diabetes have to learn to balance the meals and make the healthiest food choices. Consume the number of calories recommended by your doctor or dietician. Trans fat (hydrogenated fats) are atherogenic, while mono- and polyunsaturated fats (particularly omega-3 fatty acids) - eg, those found in fish, olive oil, nuts are protective and prevent and treat cardiovascular disease', 
    false)

WebUI.verifyTextPresent('The usual daily intake of protein should be approximately 10 to 20 percent of total caloric intake. Higher levels of dietary protein intake (>20 percent of calories from protein or >1.3 g/kg/day) have been associated with increased albuminuria, more rapid kidney function loss, and cardiovascular disease (CVD) mortality and therefore should be avoided', 
    false)

WebUI.verifyTextPresent('Sugar-sweetened beverages should be avoided.', false)

WebUI.verifyTextPresent('Alcohol - For women, no more than one drink per day; For men, no more than 2 drinks per day is recommended( one drink is equal to 12- oz beer, 5-oz glass of wine, or 1.5 oz distilled spirits)', 
    false)

WebUI.verifyTextPresent('Physical activity - Before starting any exercise regimen talk to your doctor. Your doctor may do a physical examination and tests to see if you are fit for starting an exercise regimen. After advice from your doctor, the recommended physical activity for uncomplicated diabetes mellitus is at least 150 minutes of moderate-intensity (eg, brisk walking) aerobic exercise per week.', 
    false)

WebUI.verifyTextPresent('In the absence of contraindications (eg, moderate to severe proliferative retinopathy), people with type 1 and 2 diabetes should also perform resistance training (exercise with free weights or weight machines) at least twice per week. Do not exercise and contact your doctor immediately if your blood sugar is higher than or equal to 250mg/dL.', 
    false)

WebUI.verifyTextPresent('Feet care - On a daily basis examine your feet for any skin breaks , blisters, swelling or redness. Do not walk barefoot. Always wear shoes that are not too tight and fit properly. Wear loose fitting cotton socks that need to be changed daily. Wash your feet daily with lukewarm water and mild soap.', 
    false)

WebUI.verifyTextPresent('Always wear a diabetic ID bracelet to get proper medical attention in case of emergency.', false)

WebUI.verifyTextPresent('Pharmacological management', false)

WebUI.verifyTextPresent('Medications that are used in the treatment of type 2 diabetes mellitus are Oral agents, Insulin.', 
    false)

WebUI.verifyTextPresent('Oral agents - Metformin, Acarbose, Alogliptin, Bromocriptine, Canagliflozin, Colesevelam, Dapagliflozin, Empagliflozin, Ertugliflozin, Glimepiride, Glipizide, Glyburide, Linagliptin, Miglitol, Nateglinide, pioglitazone, Repaglinide, Rosiglitazone, Saxagliptin, Sitagliptin, Tolbutamide, Albiglutide, Dulaglutide, Exenatide, Liraglutide, Lixisenatide, semaglutide.', 
    false)

WebUI.verifyTextPresent('Blood Glucose Levels - People with diabetes mellitus need to check their blood sugar levels daily and as many times as recommended by their doctor.', 
    false)

WebUI.verifyTextPresent('The American Diabetes Association suggests the following targets for most nonpregnant adults with diabetes.', 
    false)

WebUI.verifyTextPresent('Before a meal (preprandial plasma glucose): 80–130 mg/dl', false)

WebUI.verifyTextPresent('1-2 hours after the beginning of the meal (Postprandial plasma glucose): Less than 180 mg/dl.', 
    false)

