import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/rheumatoid-arthritis.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Rheumatoid Arthritis', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('RHEUMATOID ARTHRITIS (RA)', false)

WebUI.verifyTextPresent('Overview of Rheumatoid Arthritis', false)

WebUI.verifyTextPresent('Rheumatoid arthritis is an inflammatory disorder of the joints. It can also damage other body systems, including heart, eyes, skin, lungs and blood vessels. Rheumatoid arthritis is an autoimmune disorder (Immune system attacks your own body tissues) Arthritis is typically symmetrical. If uncontrolled leads to destruction of joints, causing joint deformities.', 
    false)

WebUI.verifyTextPresent('Symptoms of Rheumatoid Arthritis', false)

WebUI.verifyTextPresent('Morning stiffness or stiffness after any prolonged period of inactivity of the joints.', false)

WebUI.verifyTextPresent('Joints that are swollen, warm, and tender.', false)

WebUI.verifyTextPresent('Fever, weakness, or loss of appetite', false)

WebUI.verifyTextPresent('At first, rheumatoid arthritis affects the smaller joints - joints of hand and joints of feet. As the disease progresses it affects other joints - wrists, knees, ankles, elbows, hips, and shoulders.', 
    false)

WebUI.verifyTextPresent('Symptoms and signs of involvement of other systems includes', false)

WebUI.verifyTextPresent('Muscle pain.', false)

WebUI.verifyTextPresent('Rheumatoid nodules on the skin, skin ulcers.', false)

WebUI.verifyTextPresent('Eye pain and redness caused by episcleritis (inflammation affecting the thin layer of tissue that lies between the conjunctiva and the connective tissue layer that forms the white of the eye), scleritis (inflammation affecting the white outer coating of the eye).', 
    false)

WebUI.verifyTextPresent('Chest pain, fever, shortness of breath.', false)

WebUI.verifyTextPresent('Tightness or pressure on the middle or left side of the chest caused by coronary artery disease.', 
    false)

WebUI.verifyTextPresent('Heart attack.', false)

WebUI.verifyTextPresent('Causes of Rheumatoid Arthritis', false)

WebUI.verifyTextPresent('The exact cause of rheumatoid arthritis is not known. Combination of genetic component and environmental factors play a role in causing rheumatoid arthritis.', 
    false)

WebUI.verifyTextPresent('Risk factors', false)

WebUI.verifyTextPresent('Women are more likely to develop rheumatoid arthritis.', false)

WebUI.verifyTextPresent('Increased risk if there is a family history of rheumatoid arthritis.', false)

WebUI.verifyTextPresent('Smoking', false)

WebUI.verifyTextPresent('Environmental exposures such as asbestos or silica', false)

WebUI.verifyTextPresent('Obesity', false)

WebUI.verifyTextPresent('Complications or non-articular manifestations of Rheumatoid Arthritis', false)

WebUI.verifyTextPresent('Stroke', false)

WebUI.verifyTextPresent('Meningitis (inflammation of the membranes that cover the brain and spinal cord).', false)

WebUI.verifyTextPresent('Vasculitis (inflammation of blood vessels).', false)

WebUI.verifyTextPresent('Carpal tunnel syndrome (a condition where one of the major nerves is squeezed or compressed as it travels through the wrist).', 
    false)

WebUI.verifyTextPresent('Neuropathy (nerve damage).', false)

WebUI.verifyTextPresent('Muscle disorders.', false)

WebUI.verifyTextPresent('Episcleritis (inflammation affecting the thin layer of tissue that lies between the conjunctiva and the connective tissue layer that forms the white of the eye).', 
    false)

WebUI.verifyTextPresent('Scleritis (inflammation affecting the white outer coating of the eye).', false)

WebUI.verifyTextPresent('Coronary artery disease.', false)

WebUI.verifyTextPresent('Pericarditis (inflammation of the membrane covering the heart).', false)

WebUI.verifyTextPresent('Myocarditis (inflammation of the heart muscle).', false)

WebUI.verifyTextPresent('Heart failure.', false)

WebUI.verifyTextPresent('Atrial fibrillation (heart rhythm disorder).', false)

WebUI.verifyTextPresent('Interstitial lung disease (Disorders causing scarring of the lung tissue).', false)

WebUI.verifyTextPresent('Pleural effusion (buildup of excess fluid in between the layers that line the lungs and chest).', 
    false)

WebUI.verifyTextPresent('Pleuritis (inflammation of the layers that line the lungs and chest).', false)

WebUI.verifyTextPresent('Peripheral artery disease (Disease of blood vessels where the arteries are narrowed reducing the blood flow to the limbs).', 
    false)

WebUI.verifyTextPresent('Sjogren’s syndrome (autoimmune condition, causing dryness of mouth and eyes).', false)

WebUI.verifyTextPresent('Anemia (deficiency of red blood cells or hemoglobin in the blood).', false)

WebUI.verifyTextPresent('Felty’s syndrome (Syndrome with three conditions - rheumatoid arthritis, an enlarged spleen, and decreased white blood cells).', 
    false)

WebUI.verifyTextPresent('Lymphoma (cancer of infection fighting cells of the immune system, called lymphocytes).', false)

WebUI.verifyTextPresent('Diagnostic of Rheumatoid Arthritis', false)

WebUI.verifyTextPresent('The diagnosis of rheumatoid arthritis is made based on the signs and symptoms.Definite RA is based upon the presence of synovitis (inflammation of connective tissue that lines the joints) in at least one joint, the absence of an alternative diagnosis that better explains the synovitis, and achievement of a total score of at least 6 (of a possible 10) from the individual scores in four domains, based on American College of Rheumatology (ACR)/European League Against Rheumatism (EULAR) classification criteria.', 
    false)

WebUI.verifyTextPresent('Management of Rheumatoid Arthritis', false)

WebUI.verifyTextPresent('Lifestyle management', false)

WebUI.verifyTextPresent('Range of motion exercises after recommendation from your doctor help preserve or restore joint motion. Exercise should be performed as infrequently as once or twice a week.', 
    false)

WebUI.verifyTextPresent('Regular aerobic exercise (such as walking, swimming, cycling, and supervised cardiorespiratory aerobic conditioning) after recommendation from your doctor improves joint stability, muscle function.', 
    false)

WebUI.verifyTextPresent('Do not do weight bearing exercise to prevent bone loss.', false)

WebUI.verifyTextPresent('Resting an inflamed joint may be beneficial by taking a nap. These rest periods can alternate with exercise.', 
    false)

WebUI.verifyTextPresent('Physical therapy as recommended by your doctor.', false)

WebUI.verifyTextPresent('Occupational therapy as recommended by your doctor.', false)

WebUI.verifyTextPresent('Do not drink excess alcohol.', false)

WebUI.verifyTextPresent('Quit smoking', false)

WebUI.verifyTextPresent('Take measures to prevent falls', false)

WebUI.verifyTextPresent('Follow a healthy diet as recommended by your dietician or doctor', false)

WebUI.verifyTextPresent('Medical management', false)

WebUI.verifyTextPresent('NSAIDs (Non steroidal anti-inflammatory drugs) - Ibuprofen, Naproxen.', false)

WebUI.verifyTextPresent('Steroids - Prednisone.', false)

WebUI.verifyTextPresent('Disease modifying antirheumatic drugs - sulfasalazine, methotrexate, leflunomide, hydroxychloroquine.', 
    false)

WebUI.verifyTextPresent('Biologic agents - abatacept, adalimumab, anakinra, baricitinib, certolizumab, etanercept, golimumab, infliximab, rituximab, sarilumab, tocilizumab and tofacitinib.', 
    false)

