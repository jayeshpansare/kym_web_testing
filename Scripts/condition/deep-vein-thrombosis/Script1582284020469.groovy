import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/deep-vein-thrombosis.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Deep Vein Thrombosis', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('DEEP VEIN THROMBOSIS', false)

WebUI.verifyTextPresent('Overview of Deep Vein Thrombosis', false)

WebUI.verifyTextPresent('Deep vein thrombosis occurs when one or more of the deep veins in the body usually legs develop clots.', 
    false)

WebUI.verifyTextPresent('Symptoms of Deep Vein Thrombosis', false)

WebUI.verifyTextPresent('Leg pain.', false)

WebUI.verifyTextPresent('Leg swelling.', false)

WebUI.verifyTextPresent('Feeling of warmth in the affected leg.', false)

WebUI.verifyTextPresent('Risk factors (causes) for the development of Deep Vein Thrombosis', false)

WebUI.verifyTextPresent('Inherited blood coagulation disorders - Protein S deficiency, Protein C deficiency, antithrombin deficiency.', 
    false)

WebUI.verifyTextPresent('Malignancy.', false)

WebUI.verifyTextPresent('Surgery, especially orthopedic.', false)

WebUI.verifyTextPresent('Heart failure.', false)

WebUI.verifyTextPresent('Trauma.', false)

WebUI.verifyTextPresent('Pregnancy.', false)

WebUI.verifyTextPresent('Oral contraceptives.', false)

WebUI.verifyTextPresent('Certain cancer therapies such as lenalidomide, tamoxifen, thalidomide.', false)

WebUI.verifyTextPresent('Obesity.', false)

WebUI.verifyTextPresent('Liver disease.', false)

WebUI.verifyTextPresent('Inflammatory bowel disease.', false)

WebUI.verifyTextPresent('Nephrotic syndrome.', false)

WebUI.verifyTextPresent('Immobilization.', false)

WebUI.verifyTextPresent('Antiphospholipid syndrome (Immune system disorder that causes an increased risk of blood clots).', 
    false)

WebUI.verifyTextPresent('Paroxysmal nocturnal hemoglobinuria (A rare acquired life threatening disease characterized by destruction of red blood cells and formation of blood clots).', 
    false)

WebUI.verifyTextPresent('Complications of Deep Vein Thrombosis', false)

WebUI.verifyTextPresent('Pulmonary embolism (Life threatening condition in which there is a blockage in one of the pulmonary arteries which supply blood to the lungs).', 
    false)

WebUI.verifyTextPresent('Post-thrombotic (postphlebitic syndrome) which occurs because of chronic venous insufficiency (improper functioning of vein valves of the leg) that develop following deep vein thrombosis.', 
    false)

WebUI.verifyTextPresent('Please seek immediate medical help or call 911 if you experience signs and symptoms of pulmonary embolism such as shortness of breath, chest pain, cough, coughing up blood, rapid pulse, lightheadedness and dizziness.', 
    false)

WebUI.verifyTextPresent('Watch out for excessive bleeding while you are on anticoagulants(blood thinners) for deep vein thrombosis, as even a minor injury could become serious if you\'re taking blood thinners. Talk to your doctor about the activities you need to avoid to prevent bleeding.', 
    false)

WebUI.verifyTextPresent('Diagnosis of Deep Vein Thrombosis', false)

WebUI.verifyTextPresent('Diagnosis of deep vein thrombosis is made by ultrasound, blood test to check D-dimer, venography (X-ray that creates the image of veins in the legs and feet), computerized tomography or magnetic resonance imaging in patients with signs and symptoms of deep vein thrombosis.', 
    false)

WebUI.verifyTextPresent('Management of Deep Vein Thrombosis', false)

WebUI.verifyTextPresent('Lifestyle management', false)

WebUI.verifyTextPresent('To prevent the complications of post-thrombotic syndrome, patients with deep vein thrombosis are advised ambulation(walking) and graduated compression stockings.', 
    false)

WebUI.verifyTextPresent('Lose weight.', false)

WebUI.verifyTextPresent('Quit smoking.', false)

WebUI.verifyTextPresent('Avoid sedentary lifestyle and follow a regular exercise regimen as recommended by your doctor.', 
    false)

WebUI.verifyTextPresent('Stop every hour or so and walk around, if you are travelling a long distance by car.', false)

WebUI.verifyTextPresent('Stand or walk as often as you can, if you are on a plane.', false)

WebUI.verifyTextPresent('Do not cross your legs while sitting as this can hamper blood flow.', false)

WebUI.verifyTextPresent('Follow a healthy diet as recommended by your doctor. If you are taking warfarin for deep vein thrombosis, you need to watchout on the amount of vitamin K you eat. Vitamin K can reduce the warfarin’s effectiveness. Avoid consuming large amounts of certain foods or drinks that are rich in vitamin K. Small amounts of foods that are rich in vitamin K shouldn\'t cause a problem. Foods that are rich in vitamin K are kale, spinach, chard, broccoli, asparagus, mustard greens, brussels sprouts, green tea, collards.', 
    false)

WebUI.verifyTextPresent('Medical management', false)

WebUI.verifyTextPresent('Anticoagulation - Low molecular weight heparin, fondaparinux, rivaroxaban, apixaban, unfractionated heparin,dabigatran, warfarin.', 
    false)

WebUI.verifyTextPresent('IVC filter (inferior vena cava filter, inserted to prevent pulmonary embolism) in whom anticoagulation is contraindicated.', 
    false)

