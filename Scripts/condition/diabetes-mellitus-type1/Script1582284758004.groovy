import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/diabetes-mellitus-type1.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Diabetes Mellitus(Type 1)', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('DIABETES MELLITUS (TYPE 1)', false)

WebUI.verifyTextPresent('Overview of Diabetes Mellitus (Type 1)', false)

WebUI.verifyTextPresent('Type 1 diabetes is a condition in which the pancreas(organ in our body) produces little or no insulin. Type 1 diabetes mellitus results from autoimmune destruction of the insulin-producing cells in the pancreas.', 
    false)

WebUI.verifyTextPresent('Symptoms of Diabetes Mellitus (Type 1)', false)

WebUI.verifyTextPresent('Frequent urination, increased thirst, unintended weight loss, weakness, bedwetting in children who didn\'t wet the bed during the night, extreme hunger.', 
    false)

WebUI.verifyTextPresent('Causes of Diabetes Mellitus (Type 1)', false)

WebUI.verifyTextPresent('The exact cause of type 1 diabetes mellitus is not known but genetic, environmental factors, and exposure to viruses play a role. Body’s own immune system mistakenly destroys insulin producing cells of the pancreas.', 
    false)

WebUI.verifyTextPresent('Complications of Diabetes Mellitus (Type 1)', false)

WebUI.verifyTextPresent('Diabetic retinopathy (Retina is damaged by high blood sugar levels over time. The retina is light-sensitive tissue at the back of your eye).', 
    false)

WebUI.verifyTextPresent('Diabetic nephropathy (Damage to the kidneys).', false)

WebUI.verifyTextPresent('Diabetic neuropathy (Damage to the covering on the nerves, or blood vessels that bring oxygen to the nerves).', 
    false)

WebUI.verifyTextPresent('Atherosclerosis (a disease in which plaque builds up inside your arteries).Atherosclerosis can cause coronary artery disease, stroke, peripheral artery disease.', 
    false)

WebUI.verifyTextPresent('Foot ulcers.', false)

WebUI.verifyTextPresent('Gum disease.', false)

WebUI.verifyTextPresent('Watch out for symptoms and signs based on the complication and progression of Diabetes Mellitus (Type 1)', 
    false)

WebUI.verifyTextPresent('Tingling, numbness, burning, or shooting pain in your feet.', false)

WebUI.verifyTextPresent('Callus/calluses on the feet, loss of nails, itching, or changes in the texture of skin.', false)

WebUI.verifyTextPresent('Imbalance and unsteadiness in gait with increased likelihood of falls.', false)

WebUI.verifyTextPresent('Lightheadedness, weakness, faintness, dizziness when standing from lying or sitting position.', 
    false)

WebUI.verifyTextPresent('Constipation, diarrhea, or fecal incontinence (inability to control bowel movements).', false)

WebUI.verifyTextPresent('Feel full after eating a small amount of food.', false)

WebUI.verifyTextPresent('Sexual dysfunction in men- erectile dysfunction, decreased libido, abnormal ejaculation. Women - decreased sexual desire or pain during intercourse.', 
    false)

WebUI.verifyTextPresent('Urinary frequency and urgency, or urinary incontinence (inability to control urination).', false)

WebUI.verifyTextPresent('Recurrent urinary tract infection.', false)

WebUI.verifyTextPresent('Decreased visual acuity.', false)

WebUI.verifyTextPresent('Curtain falling or floaters in your eyes.', false)

WebUI.verifyTextPresent('Leg pain during exercise and goes away with rest.', false)

WebUI.verifyTextPresent('Diagnostic criteria for Diabetes Mellitus (Type 1)', false)

WebUI.verifyTextPresent('Classic symptoms of type 1 diabetes (increased thirst, frequent urination, weight loss, blurry vision), and a random blood glucose value of 200 mg/dL (11.1 mmol/L) or higher.', 
    false)

WebUI.verifyTextPresent('An asymptomatic individual with Fasting plasma glucose (FPG) values ≥126 mg/dL (7.0 mmol/L) or two-hour plasma glucose values of ≥200 mg/dL (11.1 mmol/L) during an oral glucose tolerance test (OGTT) or A1C values ≥6.5 percent (48 mmol/mol). In the absence of unequivocal symptomatic hyperglycemia(high blood sugar levels), the diagnosis of diabetes must be confirmed on a subsequent day by repeat measurement, repeating the same test for confirmation.', 
    false)

WebUI.verifyTextPresent('Management of Diabetes Mellitus (Type 1)', false)

WebUI.verifyTextPresent('Lifestyle management', false)

WebUI.verifyTextPresent('Diet - People with diabetes have to learn to balance the meals and make the healthiest food choices. Consume the number of calories recommended by your doctor or dietician. Trans fat (hydrogenated fats) are atherogenic, while mono- and polyunsaturated fats (particularly omega-3 fatty acids) - eg, those found in fish, olive oil, nuts are protective and prevent and treat cardiovascular disease.', 
    false)

WebUI.verifyTextPresent('Carbohydrate counting - The ideal amount of carbohydrate intake is uncertain for patients with diabetes. Carbohydrates should not be completely avoided. Choose foods with a low glycemic index and glycemic load.', 
    false)

WebUI.verifyTextPresent('Carbohydrate counting - Talk to your doctor about carbohydrate counting. Consume a predetermined total amount of carbohydrate at meals and snacks each day, calculated in grams of carbohydrate per food portion. The goal of carbohydrate counting is to promote glycemic control by implementing a consistent pattern of carbohydrate consumption with meals and snacks day to day. Carbohydrate counting focuses on adjustment of food, insulin, and activity based on patterns from detailed logs.', 
    false)

WebUI.verifyTextPresent('Fiber intake should be at least 14 grams per 1000 calories daily.', false)

WebUI.verifyTextPresent('Reduced sodium intake of 2300 mg per day. Further reduction is needed if you have hypertension and diabetes.', 
    false)

WebUI.verifyTextPresent('Sugar-sweetened beverages should be avoided.', false)

WebUI.verifyTextPresent('Alcohol - For women, no more than one drink per day; For men, no more than 2 drinks per day is recommended( one drink is equal to 12- oz beer, 5-oz glass of wine, or 1.5 oz distilled spirits).', 
    false)

WebUI.verifyTextPresent('Diabetic nephropathy (diabetic kidney disease) could progress into chronic kidney disease or end-stage renal disease if diabetes is not well controlled. Dietary recommendations for chronic kidney disease and end-stage kidney disease may vary from these recommendations. These are also not diet recommendations for refractory heart failure which could be a complication of diabetes.', 
    false)

WebUI.verifyTextPresent('Physical Activity - Before starting any exercise regimen talk to your doctor. Your doctor may do a physical examination and tests to see if you are fit for starting an exercise regimen. After advice from your doctor, the recommended physical activity for uncomplicated diabetes mellitus is at least 150 minutes of moderate-intensity (eg, brisk walking) aerobic exercise per week. In the absence of contraindications (eg, moderate to severe proliferative retinopathy), people with type 1 and 2 diabetes should also perform resistance training (exercise with free weights or weight machines) at least twice per week. Do not exercise and contact your doctor immediately if your blood sugar is higher than or equal to 250mg/dL.', 
    false)

WebUI.verifyTextPresent('Feet care - On a daily basis examine your feet for any skin breaks , blisters, swelling or redness. Do not walk barefoot. Always wear shoes that are not too tight and fit properly. Wear loose fitting cotton socks that need to be changed daily. Wash your feet daily with lukewarm water and mild soap.', 
    false)

WebUI.verifyTextPresent('Always wear a diabetic ID bracelet to get proper medical attention in case of emergency.', false)

WebUI.verifyTextPresent('Pharmacological management', false)

WebUI.verifyTextPresent('Insulin - Basal insulin and prandial insulin. Basal insulin options are long acting forms of insulin to keep the blood glucose level stable. Basal insulin options are NPH (neutral protamine hagedorn), glargine, detemir, or degludec. Prandial insulin are short acting acting which covers the increase in blood glucose levels following meals.', 
    false)

WebUI.verifyTextPresent('Blood glucose levels - People with diabetes mellitus need to check their blood sugar levels daily and as many times as recommended by their doctor.', 
    false)

WebUI.verifyTextPresent('Before a meal (preprandial plasma glucose): 80–130 mg/dl.', false)

WebUI.verifyTextPresent('1-2 hours after the beginning of the meal (Postprandial plasma glucose): Less than 180 mg/dl.', 
    false)

WebUI.verifyTextPresent('Other advice based on insulin medications', false)

WebUI.verifyTextPresent('Insulin is associated with episodes of low blood sugar levels (hypoglycemia).', false)

WebUI.verifyTextPresent('You need to Increase fluid intake before, during and after exercise.', false)

WebUI.verifyTextPresent('Measure the blood glucose before, during, and after exercise so that the changes in blood glucose can be documented and then predicted for subsequent exercise sessions.', 
    false)

WebUI.verifyTextPresent('If pre exercise glucose is less than 100 mg/dL - ingest extra food, in the form of 15 to 30 grams of quickly absorbed carbohydrate (such as glucose tablets, hard candies, or juice), which should be taken 15 to 30 minutes before exercise and approximately every 30 minutes during exercise, based on repeat blood glucose testing during the exercise.', 
    false)

WebUI.verifyTextPresent('Check your blood sugar after exercise and several hours after exercise(4 to 8 hours). If you have low blood sugar after exercise, have a snack with slow acting carbohydrate such as granola bar, trail mix, etc.', 
    false)

WebUI.verifyTextPresent('To prevent increased insulin absorption and hence hypoglycemia - Inject the insulin in a site other than the muscles to be exercised. As an example, the arm is a suitable site for cycling. On the other hand, the abdomen is preferred with tennis or racquetball where the exercise involves both the arms and legs.', 
    false)

WebUI.verifyTextPresent('Maintain consistency and duration of exercise and avoid unanticipated exercise to prevent hypoglycemia.', 
    false)

WebUI.verifyTextPresent('Do not choose carbohydrate sources high in protein to treat or prevent hypoglycemia due to potential concurrent rise in endogenous insulin.', 
    false)

WebUI.verifyTextPresent('Keep consistency in meal timings to avoid fluctuation in the blood glucose levels.', false)

WebUI.verifyTextPresent('Risks associated with alcohol consumption include hypoglycemia particularly for those taking insulin, or insulin secretagogues. So avoid drinking especially while taking these medications.', 
    false)

WebUI.verifyTextPresent('Always carry 15 to 20 grams of sugar with you all the time to use it during the episodes of hypoglycemia.', 
    false)

WebUI.verifyTextPresent('Check your blood sugar before driving to avoid having an episode of hypoglycemia during driving.', 
    false)

WebUI.verifyTextPresent('Wear diabetes bracelet all the time in case of emergency.', false)

WebUI.verifyTextPresent('Talk to your doctor if glucagon is right for you in case of an episode of clinically significant low blood sugar levels. Glucagon is prescribed for all individuals at increased risk of clinically significant hypoglycemia, defined as blood glucose ,54 mg/dL (3.0 mmol/L), so it is available should it be needed. Caregivers, school personnel, or family members of these individuals should know where it is and when and how to administer it. Glucagon administration is not limited to health care professionals.', 
    false)

