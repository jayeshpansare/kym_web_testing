import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/polycystic-ovary-syndrome.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Polycystic Ovary Syndrome', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('POLYCYSTIC OVARY SYNDROME (PCOS)', false)

WebUI.verifyTextPresent('Overview of Polycystic Ovary Syndrome', false)

WebUI.verifyTextPresent('Polycystic ovary syndrome is a hormonal disorder with menstrual irregularity and androgen (male hormone) excess in women. Numerous follicles (fluid collections) may develop in the ovaries. Ovaries fail to regularly release eggs.', 
    false)

WebUI.verifyTextPresent('Symptoms of Polycystic Ovary Syndrome', false)

WebUI.verifyTextPresent('Menstrual irregularity - infrequent menstrual periods, or absent menstrual periods.', false)

WebUI.verifyTextPresent('Obesity or overweight.', false)

WebUI.verifyTextPresent('Infertility', false)

WebUI.verifyTextPresent('Excess facial and body hair (also called hirsutism).', false)

WebUI.verifyTextPresent('Severe acne and male pattern baldness', false)

WebUI.verifyTextPresent('Causes of Polycystic Ovary Syndrome', false)

WebUI.verifyTextPresent('The exact cause of polycystic ovary syndrome is not known but heredity and low grade inflammation may play a role in the development of polycystic ovary syndrome.', 
    false)

WebUI.verifyTextPresent('Complications of Polycystic Ovary Syndrome', false)

WebUI.verifyTextPresent('Obesity', false)

WebUI.verifyTextPresent('Gestational diabetes and pregnancy induced hypertension.', false)

WebUI.verifyTextPresent('Infertility', false)

WebUI.verifyTextPresent('Miscarriage', false)

WebUI.verifyTextPresent('Type 2 diabetes mellitus or prediabetes.', false)

WebUI.verifyTextPresent('Endometrial cancer (cancer of the uterus).', false)

WebUI.verifyTextPresent('Depression and anxiety.', false)

WebUI.verifyTextPresent('Sleep apnea.', false)

WebUI.verifyTextPresent('Eating disorders.', false)

WebUI.verifyTextPresent('Nonalcoholic fatty liver.', false)

WebUI.verifyTextPresent('Symptoms and Signs based on complications of Polycystic Ovary Syndrome', false)

WebUI.verifyTextPresent('Symptoms of type 2 diabetes mellitus - frequent urination, increased thirst, increased hunger, fatigue.', 
    false)

WebUI.verifyTextPresent('Snoring, excessive daytime sleepiness, morning headaches - Obstructive sleep apnea is common in women with polycystic ovary syndrome.', 
    false)

WebUI.verifyTextPresent('Depression and anxiety are commonly associated in women with polycystic ovary syndrome.', false)

WebUI.verifyTextPresent('Diagnosis of Polycystic Ovary Syndrome', false)

WebUI.verifyTextPresent('The diagnosis of polycystic ovary syndrome is based on clinical features and exclusion of other conditions that mimic polycystic ovary syndrome.', 
    false)

WebUI.verifyTextPresent('Management of Polycystic Ovary Syndrome', false)

WebUI.verifyTextPresent('Lifestyle management', false)

WebUI.verifyTextPresent('Low calorie diet and moderate intensity exercise after recommendation from your doctor helps in weight reduction.', 
    false)

WebUI.verifyTextPresent('If you have prediabetes or diabetes as a complication of polycystic ovary syndrome, follow the diet recommended for these conditions.', 
    false)

WebUI.verifyTextPresent('People with diabetes have to learn to balance the meals and make the healthiest food choices. Consume the number of calories recommended by your doctor or dietician. ', 
    false)

WebUI.verifyTextPresent('Trans fat (hydrogenated fats) are atherogenic, while mono- and polyunsaturated fats (particularly omega-3 fatty acids) - eg: those found in fish, olive oil, nuts are protective and prevent and treat cardiovascular disease.', 
    false)

WebUI.verifyTextPresent('Carbohydrate counting - The ideal amount of carbohydrate intake is uncertain for patients with diabetes. Carbohydrates should not be completely avoided. Choose foods with a low glycemic index and glycemic load.', 
    false)

WebUI.verifyTextPresent('The usual daily intake of protein should be approximately 10 to 20 percent of total caloric intake. Higher levels of dietary protein intake (>20 percent of calories from protein or >1.3 g/kg/day) have been associated with increased albuminuria, more rapid kidney function loss, and cardiovascular disease (CVD) mortality and therefore should be avoided.', 
    false)

WebUI.verifyTextPresent('Fiber intake should be at least 14 grams per 1000 calories daily.', false)

WebUI.verifyTextPresent('Reduced sodium intake of 2300 mg per day. Further reduction is needed if you have hypertension.', 
    false)

WebUI.verifyTextPresent('Sugar-sweetened beverages should be avoided.', false)

WebUI.verifyTextPresent('If you are using sugar alcohols - When calculating the carbohydrate content of foods, one-half of the sugar alcohol content should be counted in the total carbohydrate content of the food.', 
    false)

WebUI.verifyTextPresent('Alcohol - For women, no more than one drink per day; For men, no more than 2 drinks per day is recommended (one drink is equal to 12- oz beer, 5-oz glass of wine, or 1.5 oz distilled spirits).', 
    false)

WebUI.verifyTextPresent('Medical management', false)

WebUI.verifyTextPresent('Combined estrogen and progesterone pills for menstrual irregularities,acne and hirsutism.', false)

WebUI.verifyTextPresent('Metformin', false)

WebUI.verifyTextPresent('Progestin', false)

WebUI.verifyTextPresent('Eflornithine hydrochloride.', false)

WebUI.verifyTextPresent('Spironolactone', false)

WebUI.verifyTextPresent('Cyproterone acetate.', false)

