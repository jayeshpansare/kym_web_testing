import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/sickle-cell-anemia.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Sickle Cell Anemia', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('SICKLE CELL ANEMIA', false)

WebUI.verifyTextPresent('Overview of Sickle Cell Anemia', false)

WebUI.verifyTextPresent('Sickle cell anemia is a form of inherited anemia in which the red blood cells become rigid and sticky and are shaped like sickles.', 
    false)

WebUI.verifyTextPresent('These rigid red blood cells get stuck in small blood vessels and block the blood flow and oxygen to different parts of the body. Occlusion of the blood vessels by red blood cells leads to painful episodes.', 
    false)

WebUI.verifyTextPresent('Sickle cell anemia is caused by a mutation in the gene that makes heme and is an autosomal recessive disease. This means that both mother and father must pass the defective form of the gene for the child to be affected by sickle cell anemia.', 
    false)

WebUI.verifyTextPresent('Symptoms of Sickle Cell Anemia', false)

WebUI.verifyTextPresent('Anemia - Fatigue, shortness of breath, skin pallor, fast heartbeat, dizziness.', false)

WebUI.verifyTextPresent('Painful episodes - Pain in the chest, abdomen and joints because of the obstruction of the blood vessels in various parts of the body. Pain may last for few hours to few weeks. There is significant variability in the severity and frequency of painful episodes.', 
    false)

WebUI.verifyTextPresent('Frequent infections - Spleen which plays a role in fighting infections is damaged by the sickle shaped red blood cells, leaving sickle cell patients more prone to infections.', 
    false)

WebUI.verifyTextPresent('Delayed growth - Delayed growth is seen infants and children because of decreased supply of oxygen and nutrients.', 
    false)

WebUI.verifyTextPresent('Vision problems - Sickle cells block the tiny blood vessels that supply your eyes.', false)

WebUI.verifyTextPresent('Complications of Sickle Cell Anemia', false)

WebUI.verifyTextPresent('Anemia', false)

WebUI.verifyTextPresent('Frequent infections - pneumonia, meningitis, acute chest syndrome (sickling in the small blood vessels of the lungs).', 
    false)

WebUI.verifyTextPresent('Kidney infarction (tissue death due to inadequate blood because of the occlusion).', false)

WebUI.verifyTextPresent('Bone infarction (tissue death due to inadequate blood because of the occlusion).', false)

WebUI.verifyTextPresent('Myocardial infarction (tissue death due to inadequate blood because of the occlusion).', false)

WebUI.verifyTextPresent('Stroke', false)

WebUI.verifyTextPresent('Priapism (persistent penile erection that is not related to sexual interest or desire).', false)

WebUI.verifyTextPresent('Venous thromboembolism (clots in the deep veins).', false)

WebUI.verifyTextPresent('Chronic complications such as chronic pain, neurologic deficits, seizure disorder, asthma, pulmonary hypertension (hypertension in the lungs), osteoporosis, iron overload, chronic leg ulcers, proliferative retinopathy (new but weak blood vessels begin to form on the retina to help restore blood supply), kidney impairment, hypertension.', 
    false)

WebUI.verifyTextPresent('Seek immediate medical help if you or your child experience any of these symptoms:', false)

WebUI.verifyTextPresent('Severe pain in the abdomen, chest, bones, or joints.', false)

WebUI.verifyTextPresent('Fever', false)

WebUI.verifyTextPresent('Pale skin or nail beds', false)

WebUI.verifyTextPresent('Yellow discoloration of the skin or whites of the eyes', false)

WebUI.verifyTextPresent('Swelling of the hands or feet', false)

WebUI.verifyTextPresent('Abdominal swelling', false)

WebUI.verifyTextPresent('Signs and symptoms of stroke - Confusion; one-sided paralysis or weakness in the face, arms or legs; sudden vision problems or unexplained numbness; trouble walking or talking; or a headache.', 
    false)

WebUI.verifyTextPresent('Patients with priapism - If the erection lasts longer than four hours.', false)

WebUI.verifyTextPresent('Screening and diagnosis of Sickle Cell Anemia', false)

WebUI.verifyTextPresent('All newborns are screened for sickle cell anemia with a blood test in the hospital to screen for hemoglobin S.', 
    false)

WebUI.verifyTextPresent('Adults and older children can also be tested with blood test.', false)

WebUI.verifyTextPresent('If the blood test comes positive for hemoglobin S, additional tests are done to differentiate if you have disease with two sickle cell genes, or if you are carrier with just one sickle cell gene.', 
    false)

WebUI.verifyTextPresent('Other tests that are done - red blood cell count, tests for complications of the disease.', false)

WebUI.verifyTextPresent('Genetic counseling if you carry the gene for sickle cell anemia.', false)

WebUI.verifyTextPresent('Prenatal testing - Diagnosis in the unborn baby is done with sampling the amniotic fluid (Fluid that surrounds the baby). This is done if you or your partner have sickle cell disease (carry 2 sickle cell genes) or sickle cell trait (carry one sickle cell gene) and are also referred to a genetic counselor.', 
    false)

WebUI.verifyTextPresent('Management of Sickle Cell Anemia', false)

WebUI.verifyTextPresent('Lifestyle management', false)

WebUI.verifyTextPresent('Follow a diet recommended by your doctor which should include whole grains, fruits, and vegetables.', 
    false)

WebUI.verifyTextPresent('Take folic acid supplements as directed by your doctor.', false)

WebUI.verifyTextPresent('People with sickle cell anemia are recommended to keep themselves well hydrated with plenty of water. Dehydration can increase the risk of sickle cell crisis (painful episode).', 
    false)

WebUI.verifyTextPresent('Avoid exposure to extreme cold or heat. Extreme variations in temperature could trigger sickle cell crisis.', 
    false)

WebUI.verifyTextPresent('Exercise as recommended by your doctor.', false)

WebUI.verifyTextPresent('Wear properly fitting shoes to prevent leg ulcers from happening.', false)

WebUI.verifyTextPresent('All people with sickle cell anemia should receive age appropriate vaccinations.', false)

WebUI.verifyTextPresent('Priapism is managed with medications to relieve pain, hydration, exercise, warm or cold compresses, and masturbation with ejaculation.', 
    false)

WebUI.verifyTextPresent('Medical management', false)

WebUI.verifyTextPresent('Penicillin is given in early childhood to prevent infection.', false)

WebUI.verifyTextPresent('Hydroxyurea is mainstay for prevention and treatment of complications of sickle cell anemia.', 
    false)

WebUI.verifyTextPresent('L- glutamine is also used to reduce acute complications.', false)

WebUI.verifyTextPresent('Hematopoietic cell transplantation is the only curative treatment in people with sickle cell anemia.', 
    false)

WebUI.verifyTextPresent('Blood transfusions are used to treat and prevent complications of sickle cell disease.', false)

WebUI.verifyTextPresent('Pain relieving medication for sickle cell crisis.', false)

