import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/gout.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Gout', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('GOUT', false)

WebUI.verifyTextPresent('Overview of Gout', false)

WebUI.verifyTextPresent('Gout is an inflammation and accumulation of urate crystals in the joint/joints. Gout often affects the joint at the base of the big toe and is characterized by pain, swelling, tenderness or redness.', 
    false)

WebUI.verifyTextPresent('There is an increase in the blood uric acid levels which is a predisposing factor for gout.', false)

WebUI.verifyTextPresent('The formation of monosodium urate (MSU) crystals and/or inflammatory responses to these crystals play a role in whether a person with increased blood uric acid levels will develop gout. Blood uric acid levels exceeds 6.8 mg/dL (approximately 400 micromol/L).', 
    false)

WebUI.verifyTextPresent('Symptoms of Gout', false)

WebUI.verifyTextPresent('Intolerable joint pain that usually involves the large joint of your big toe, but it can occur in any joint. Gout flare may last 3 to10 days.', 
    false)

WebUI.verifyTextPresent('Swelling, warmth and tenderness of the joint.', false)

WebUI.verifyTextPresent('Limited range of motion.', false)

WebUI.verifyTextPresent('Risk factors for Gout', false)

WebUI.verifyTextPresent('Obesity.', false)

WebUI.verifyTextPresent('Certain medical conditions - Diabetes, metabolic syndrome, untreated hypertension, heart disease, kidney disease.', 
    false)

WebUI.verifyTextPresent('Medications such as thiazide diuretics, low dose aspirin.', false)

WebUI.verifyTextPresent('Family history of gout.', false)

WebUI.verifyTextPresent('Factors Provoking Gout Flares', false)

WebUI.verifyTextPresent('Trauma', false)

WebUI.verifyTextPresent('Surgery', false)

WebUI.verifyTextPresent('Starvation', false)

WebUI.verifyTextPresent('Fatty foods and other dietary triggers', false)

WebUI.verifyTextPresent('Dehydration', false)

WebUI.verifyTextPresent('Ingestion of drugs affecting (raising or lowering) serum urate concentrations such as thiazide diuretics, loop diuretics, low-dose aspirin or allopurinol, uricosuric agents.', 
    false)

WebUI.verifyTextPresent('Alcohol consumption, including beer, spirits, and wine.', false)

WebUI.verifyTextPresent('Complications of Gout', false)

WebUI.verifyTextPresent('Kidney stones which may present as pain in the abdomen,flank, or groin.', false)

WebUI.verifyTextPresent('Chronic urate nephropathy (deposition of urate crystals in parts of the kidney).', false)

WebUI.verifyTextPresent('Diagnosis of Gout', false)

WebUI.verifyTextPresent('Joint fluid analysis- Fluid is obtained from joints and visualized by direct examination using compensated polarized light microscopy. The presence of needle shaped monosodium urate crystals in the synovial fluid is diagnostic of gout. Joint fluid is also looked for white blood cell count.', 
    false)

WebUI.verifyTextPresent('Blood test to check creatinine levels and uric acid levels.', false)

WebUI.verifyTextPresent('X-ray of the joint to rule out other causes of arthritis.', false)

WebUI.verifyTextPresent('Ultrasound can be used sometimes to detect urate crystals in a joint.', false)

WebUI.verifyTextPresent('Dual energy CT scan.', false)

WebUI.verifyTextPresent('Management of Gout', false)

WebUI.verifyTextPresent('Lifestyle management', false)

WebUI.verifyTextPresent('Prevention of gout flare during intercritical (between flares) period', false)

WebUI.verifyTextPresent('Follow a healthy diet and exercise program as recommended by your doctor which is aimed at achieving weight reduction (at a rate of three to five pounds per month) toward ideal body weight for overweight people.', 
    false)

WebUI.verifyTextPresent('Dietary moderation in foods that are rich in proteins such as seafood, shellfish, bacon, turkey, and organ meats.', 
    false)

WebUI.verifyTextPresent('Get proteins from low fat dairy products. Low fat dairy products have a protective effect against gout.', 
    false)

WebUI.verifyTextPresent('Stay well hydrated with plenty of fluid intake.', false)

WebUI.verifyTextPresent('Alcohol-containing beverages and sugar-sweetened juices or beverages containing high-fructose corn syrup should be minimized or avoided.', 
    false)

WebUI.verifyTextPresent('Pharmacologic management', false)

WebUI.verifyTextPresent('Choice of agent for treatment of a flare is based upon relative safety and efficacy of each agent in individual people with gout.', 
    false)

WebUI.verifyTextPresent('Oral glucocorticoids (prednisone, or prednisolone), or intraarticular glucocorticoids (Triamcinolone acetonide, or methylprednisolone acetate), or parenteral glucocorticoids (Triamcinolone acetonide).', 
    false)

WebUI.verifyTextPresent('NSAIDs - Naproxen, Indomethacin, ibuprofen, diclofenac, meloxicam, celecoxib.', false)

WebUI.verifyTextPresent('Cochicine.', false)

WebUI.verifyTextPresent('Urate lowering therapy is recommended in people with a history of gout who have frequent and disabling gout flares. Allopurinol, probenecid, febuxostat, lesinurad, benzbromarone are uricosuric agents used in lowering the blood uric acid levels.', 
    false)

