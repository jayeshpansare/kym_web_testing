import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/abdominal-aortic-aneurysm.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Abdominal Aortic Aneurysm', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('ABDOMINAL AORTIC ANEURYSM (AAA)', false)

WebUI.verifyTextPresent('Overview of Abdominal Aortic Aneurysm', false)

WebUI.verifyTextPresent('Abdominal aortic aneurysm is a condition that occurs when an area of the aorta becomes very large or balloons out. Aorta is the main blood vessel in our body that supplies blood to the abdomen, pelvis, and legs. An aortic diameter >3.0 cm is generally considered aneurysmal, in most adults.', 
    false)

WebUI.verifyTextPresent('Symptoms of Abdominal Aortic Aneurysm', false)

WebUI.verifyTextPresent('Majority of the people are asymptomatic and aneurysm become apparent as a result of screening or be discovered incidentally on routine physical examination.', 
    false)

WebUI.verifyTextPresent('Abdominal, back, or flank pain', false)

WebUI.verifyTextPresent('Severe pain, hypotension, and a pulsatile abdominal mass indicating a rupture of aortic aneurysm.', 
    false)

WebUI.verifyTextPresent('Seek immediate medical help, If you have pain, especially if pain is sudden and severe.', false)

WebUI.verifyTextPresent('Risk factors', false)

WebUI.verifyTextPresent('Cigarette smoking, older age, men, hypertension, Atherosclerosis(buildup of plaque in the blood vessels), family history of abdominal aortic aneurysm, caucasian race, other large artery aneurysms.', 
    false)

WebUI.verifyTextPresent('Screening for Abdominal Aortic Aneurysm', false)

WebUI.verifyTextPresent('A one-time screening for AAA is recommended for men ages 65 to 75 who have smoked.', false)

WebUI.verifyTextPresent('In men ages 65 to 75 who have never smoked but who have a first-degree relative who required repair of an AAA or died from a ruptured AAA.', 
    false)

WebUI.verifyTextPresent('Ultrasonography is the imaging test of choice for screening AAA.', false)

WebUI.verifyTextPresent('Complications of Abdominal Aortic Aneurysm', false)

WebUI.verifyTextPresent('Rupture of AAA can cause severe internal bleeding which can present as severe and sudden abdominal or back pain, low blood pressure and fast pulse.', 
    false)

WebUI.verifyTextPresent('Aneurysm also puts you at risk of developing blood clots and the blood clots can break loose from the inside wall of an aneurysm and blocks a blood vessel elsewhere in the body.', 
    false)

WebUI.verifyTextPresent('Diagnosis of Abdominal Aortic Aneurysm', false)

WebUI.verifyTextPresent('Ultrasonography', false)

WebUI.verifyTextPresent('Computerized tomography(CT scan)', false)

WebUI.verifyTextPresent('Magnetic resonance imaging (MRI)', false)

WebUI.verifyTextPresent('Management of Abdominal Aortic Aneurysm', false)

WebUI.verifyTextPresent('Lifestyle management', false)

WebUI.verifyTextPresent('To prevent aortic aneurysm from happening or getting worse.', false)

WebUI.verifyTextPresent('Quit smoking or chewing tobacco, avoid secondhand smoke.', false)

WebUI.verifyTextPresent('Healthy diet consisting of fruits and vegetables, whole grains, poultry, fish and low-fat dairy products, low in saturated fat; trans fats; and salt.', 
    false)

WebUI.verifyTextPresent('After advice from your doctor at least 150 minutes a week of moderate aerobic activity is recommended. Talk to your doctor about the type of activities that are right for you.', 
    false)

WebUI.verifyTextPresent('Medical management', false)

WebUI.verifyTextPresent('Aneurysm repair can be accomplished using open surgical or endovascular techniques.', false)

WebUI.verifyTextPresent('Asymptomatic aneurysm', false)

WebUI.verifyTextPresent('The decision to repair AAA depends upon the diameter of the aneurysm at diagnosis and the person’s medical comorbidities.', 
    false)

WebUI.verifyTextPresent('For people with asymptomatic AAA who are not candidates for repair or refuse repair, routine surveillance is made with ultrasonography to check the size of the aneurysm.The frequency of interval varies from 6 months to one year.', 
    false)

WebUI.verifyTextPresent('Ruptured aneurysm', false)

WebUI.verifyTextPresent('The decision and timing to do endovascular repair depend on whether the person with rupture is hemodynamically stable or not.', 
    false)

