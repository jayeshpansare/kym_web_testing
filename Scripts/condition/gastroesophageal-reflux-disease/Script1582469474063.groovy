import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/gastroesophageal-reflux-disease.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Gastroesophageal Reflux Disease', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('GASTROESOPHAGEAL REFLUX DISEASE (GERD)', false)

WebUI.verifyTextPresent('Overview of Gastroesophageal Reflux Disease', false)

WebUI.verifyTextPresent('Gastroesophageal reflux disease is a condition where the stomach acid leak back into the esophagus from the stomach. Esophagus is the tube that connects the mouth to the stomach. Gastroesophageal reflux disease (GERD) happens when a muscle at the end of the esophagus does not close properly.', 
    false)

WebUI.verifyTextPresent('Symptoms of Gastroesophageal Reflux Disease', false)

WebUI.verifyTextPresent('Heartburn - burning sensation in the chest or throat.', false)

WebUI.verifyTextPresent('Regurgitation - The perception of flow of refluxed stomach contents into the mouth or throat.', 
    false)

WebUI.verifyTextPresent('Other symptoms may include difficulty swallowing food, cough, wheezing, hoarseness, water brash (there is an excessive accumulation of saliva in the lower part of the esophagus which regurgitates back often mixed with acid)', 
    false)

WebUI.verifyTextPresent('Risk factors for Gastroesophageal Reflux Disease', false)

WebUI.verifyTextPresent('Factors that can aggravate acid reflux - drinking coffee or alcohol, fatty or fried foods, taking certain medications like aspirin, smoking.', 
    false)

WebUI.verifyTextPresent('Conditions that can increase your risk of GERD are - Obesity, pregnancy, connective tissue disorders.', 
    false)

WebUI.verifyTextPresent('Complications of Gastroesophageal Reflux Disease', false)

WebUI.verifyTextPresent('Barrett’s esophagus - Barrett’s esophagus is a disorder in which the lining of the esophagus is damaged by stomach acid.', 
    false)

WebUI.verifyTextPresent('Esophageal stricture - Esophageal stricture is narrowing of the esophagus (the tube from the mouth to the stomach).', 
    false)

WebUI.verifyTextPresent('Esophageal cancer.', false)

WebUI.verifyTextPresent('Exacerbation of asthma.', false)

WebUI.verifyTextPresent('Laryngitis - Inflammation of voice box.', false)

WebUI.verifyTextPresent('Diagnosis of Gastroesophageal Reflux Disease', false)

WebUI.verifyTextPresent('Diagnosis of GERD can often be based on symptoms of heartburn and/or regurgitation.', false)

WebUI.verifyTextPresent('Other symptoms without the symptoms of heartburn and/or regurgitation need further evaluation before attributing the symptoms to GERD.', 
    false)

WebUI.verifyTextPresent('Management of Gastroesophageal Reflux Disease', false)

WebUI.verifyTextPresent('Lifestyle management', false)

WebUI.verifyTextPresent('Elimination of the foods that trigger the symptoms of GERD. The foods that generally trigger symptoms are fatty foods, caffeine, chocolate, spicy foods, food with high fat content, carbonated beverages, and peppermint.', 
    false)

WebUI.verifyTextPresent('Elevation of the head of the bed especially in individuals who have symptoms at night.', false)

WebUI.verifyTextPresent('Avoidance of meals two to three hours before bedtime reduces the symptoms of GERD.', false)

WebUI.verifyTextPresent('Avoid wearing tight fitting garments.', false)

WebUI.verifyTextPresent('Avoid tobacco, alcohol.', false)

WebUI.verifyTextPresent('Weight loss for individuals who are overweight.', false)

WebUI.verifyTextPresent('Medical management', false)

WebUI.verifyTextPresent('Antacids - aluminium hydroxide, calcium carbonate.', false)

WebUI.verifyTextPresent('Surface agents and alginates - Sucralfate, sodium alginate.', false)

WebUI.verifyTextPresent('Histamine 2 receptor antagonist - Ranitidine, famotidine, cimetidine, nizatidine.', false)

WebUI.verifyTextPresent('Proton pump inhibitors - Omeprazole, lansoprazole, esomeprazole, dexlansoprazole, rabeprazole, pantoprazole.', 
    false)

