import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/chronic-obstructive-pulmonary-disease.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Chronic Obstructive Pulmonary Disease', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('Chronic Obstructive Pulmonary Disease (COPD)', false)

WebUI.verifyTextPresent('Overview of COPD', false)

WebUI.verifyTextPresent('COPD is a group of diseases that cause airflow blockage and this makes it hard to breathe.', false)

WebUI.verifyTextPresent('The three subtypes of COPD are emphysema, chronic bronchitis and chronic obstructive asthma.', 
    false)

WebUI.verifyTextPresent('Chronic inflammation in COPD is usually caused by significant exposure to noxious particles or gases. Chronic inflammation causes structural changes in the airways.', 
    false)

WebUI.verifyTextPresent('In emphysema there is destruction of the fragile walls and elastic fibers of the alveoli (tiny air sacs).', 
    false)

WebUI.verifyTextPresent('Whereas in chronic bronchitis there is narrowing of bronchial tubes because of inflammation. This inflammation produces more mucus. You develop a chronic cough trying to clear your airways. COPD can also be due to a genetic disorder called alpha-1-antitrypsin deficiency.', 
    false)

WebUI.verifyTextPresent('Symptoms of COPD', false)

WebUI.verifyTextPresent('Chronic bronchitis is characterized by daily cough and mucus (sputum) production at least three months a year for two consecutive years.', 
    false)

WebUI.verifyTextPresent('Other symptoms of COPD are - shortness of breath; chest pain; wheezing; frequent infection with fever and exacerbations; unintended weight loss; weakness; swelling in ankles, feet, or legs', 
    false)

WebUI.verifyTextPresent('Risk factors for COPD', false)

WebUI.verifyTextPresent('Cigarette smoking, other risk factors include passive smoke and biomass fuel use.', false)

WebUI.verifyTextPresent('Complications of COPD', false)

WebUI.verifyTextPresent('Respiratory infections (flu, pneumonia), pulmonary hypertension (high blood pressure in the arteries that bring blood to your lungs), lung cancer, depression, heart disease.', 
    false)

WebUI.verifyTextPresent('Managing exacerbations', false)

WebUI.verifyTextPresent('Seek immediate medical help if you notice sustained increase in the symptoms such as coughing, difficulty breathing, change in the mucus or increased mucus production.', 
    false)

WebUI.verifyTextPresent('An exacerbation may lead to lung failure if you don\'t receive prompt treatment.', false)

WebUI.verifyTextPresent('Diagnosis of COPD', false)

WebUI.verifyTextPresent('Evaluation is done in people with history of cigarette smoking, or indoor biomass smoke presenting with symptoms and signs of COPD.', 
    false)

WebUI.verifyTextPresent('All patients presenting with clinical features are evaluated with spirometry (pulmonary function test) and selected patients have laboratory testing and imaging studies such as chest X ray, computerized tomography scan.', 
    false)

WebUI.verifyTextPresent('Arterial blood gas analysis.', false)

WebUI.verifyTextPresent('Testing for alpha-1 antitrypsin (AAT) deficiency to determine if the cause of COPD is genetic.', 
    false)

WebUI.verifyTextPresent('Management of COPD', false)

WebUI.verifyTextPresent('Lifestyle management', false)

WebUI.verifyTextPresent('Quit smoking. Smoking cessation can reduce the rate of decline in lung function that occurs in smokers with COPD.', 
    false)

WebUI.verifyTextPresent('Weight loss for overweight and obese people with COPD can help improve exercise tolerance and reduce shortness of breath.', 
    false)

WebUI.verifyTextPresent('Follow a healthy diet as recommended by your doctor.', false)

WebUI.verifyTextPresent('Avoid exposure to chemical fumes and dust.', false)

WebUI.verifyTextPresent('Influenza and pneumococcal vaccination can help reduce infections and exacerbations. Talk to your doctor about these vaccinations.', 
    false)

WebUI.verifyTextPresent('Pulmonary rehabilitation program is a program in which a variety of specialists will work with you to meet your needs, increase your ability to participate in everyday activities and improve your quality of life. Talk to your doctor about referral to a program.', 
    false)

WebUI.verifyTextPresent('Medical management', false)

WebUI.verifyTextPresent('Bronchodilators', false)

WebUI.verifyTextPresent('Short-acting bronchodilators include albuterol, levalbuterol, and ipratropium.', false)

WebUI.verifyTextPresent('The long-acting bronchodilators include tiotropium, salmeterol, formoterol, arformoterol, aclidinium, and indacaterol.', 
    false)

WebUI.verifyTextPresent('Inhaled Steroids', false)

WebUI.verifyTextPresent('Budesonide', false)

WebUI.verifyTextPresent('Fluticasone', false)

WebUI.verifyTextPresent('Oral Steroids', false)

WebUI.verifyTextPresent('Phosphodiesterase 4 Inhibitors - Roflumilast', false)

WebUI.verifyTextPresent('Theophylline', false)

WebUI.verifyTextPresent('Antibiotics help treat exacerbations of COPD', false)

WebUI.verifyTextPresent('Oxygen Therapy - Oxygen therapy is prescribed to people who do not have enough oxygen in their blood. There are several oxygen delivering devices. Talk to your doctor about your needs and options. Oxygen therapy can improve quality of life and is only proven therapy to extend life.', 
    false)

