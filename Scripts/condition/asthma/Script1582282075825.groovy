import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/asthma.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Asthma', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('ASTHMA', false)

WebUI.verifyTextPresent('Overview of Asthma', false)

WebUI.verifyTextPresent('Asthma is a condition where the airways are swollen, inflamed and narrow. Airways produces extra mucus. This can make breathing difficult.', 
    false)

WebUI.verifyTextPresent('Symptoms of Asthma', false)

WebUI.verifyTextPresent('Coughing, wheezing or whistling sound while breathing, shortness of breath.', false)

WebUI.verifyTextPresent('Risk factors for Asthma', false)

WebUI.verifyTextPresent('Having a family member such as a parent or sibling with asthma.', false)

WebUI.verifyTextPresent('Cigarette smoking.', false)

WebUI.verifyTextPresent('Secondhand smoke.', false)

WebUI.verifyTextPresent('Exposure to exhaust fumes or other types of pollution.', false)

WebUI.verifyTextPresent('Causes of Asthma', false)

WebUI.verifyTextPresent('Asthma is probably due to a combination of genetic and environmental factors. There are various irritants and substances that can trigger asthma.', 
    false)

WebUI.verifyTextPresent('Airborne substances such as pollen, mold, pet dander, cockroach waste.', false)

WebUI.verifyTextPresent('Cold air.', false)

WebUI.verifyTextPresent('Certain medications such as aspirin, beta blockers, ibuprofen, naproxen.', false)

WebUI.verifyTextPresent('Gastroesophageal reflux disease.', false)

WebUI.verifyTextPresent('Stress', false)

WebUI.verifyTextPresent('Exercise', false)

WebUI.verifyTextPresent('Classification of Asthma based on symptoms', false)

WebUI.verifyTextPresent('Mild intermittent - Mild symptoms up to two days a week and up to two nights a month.', false)

WebUI.verifyTextPresent('Mild persistent - Symptoms more than twice a week, but no more than once in a single day.', false)

WebUI.verifyTextPresent('Moderate persistent - Symptoms once a day and more than one night a week.', false)

WebUI.verifyTextPresent('Severe persistent - Symptoms throughout the day on most days and frequently at night.', false)

WebUI.verifyTextPresent('Diagnosis of Asthma', false)

WebUI.verifyTextPresent('Asthma is confirmed by spirometry (pulmonary function testing) in patients with clinical features of asthma.', 
    false)

WebUI.verifyTextPresent('Management of Asthma', false)

WebUI.verifyTextPresent('Lifestyle management', false)

WebUI.verifyTextPresent('Home monitoring of the Peak expiratory flow rate may be helpful in people with moderate to severe asthma.', 
    false)

WebUI.verifyTextPresent('Asthma action plan - Asthma action plan is a detailed written document created by your doctor or healthcare team with instructions for taking medications and managing asthma attacks.', 
    false)

WebUI.verifyTextPresent('Identify outdoor allergens or irritants that triggers asthma and take steps to avoid the triggers.', 
    false)

WebUI.verifyTextPresent('Get vaccinated for flu and pneumonia.', false)

WebUI.verifyTextPresent('Medical management', false)

WebUI.verifyTextPresent('Short acting beta agonists - Albuterol, levalbuterol.', false)

WebUI.verifyTextPresent('Inhaled glucocorticoids - Fluticasone, budesonide, flunisolide, beclomethasone, mometasone.', false)

WebUI.verifyTextPresent('Long acting beta agonists - Salmeterol, formoterol,Theophylline.', false)

WebUI.verifyTextPresent('Leukotriene modifiers - montelukast, zafirlukast, zileuton.', false)

