import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/hyperthyroidism.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Hyperthyroidism', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('HYPERTHYROIDISM', false)

WebUI.verifyTextPresent('Overview of Hyperthyroidism', false)

WebUI.verifyTextPresent('Hyperthyroidism is a condition that occurs when there is too much of the thyroxine synthesis by the thyroid gland, or destruction of thyroid tissue with release of preformed hormone into the circulation, or an extrathyroidal source of thyroid hormone.', 
    false)

WebUI.verifyTextPresent('Two main hormones produced by thyroid gland are T3 (triiodothyronine) and T4 (thyroxine). These hormones help control the body temperature, maintain the rate at which the body uses fats and carbohydrates, helps regulate the amount of calcium in the blood, influence the heart rate.', 
    false)

WebUI.verifyTextPresent('Symptoms and signs of Hyperthyroidism', false)

WebUI.verifyTextPresent('Sweating, heat intolerance, tremors.', false)

WebUI.verifyTextPresent('Itching, hives, loosening of the nails.', false)

WebUI.verifyTextPresent('Thinning of hair, bald patches.', false)

WebUI.verifyTextPresent('Hyperpigmented, raised violaceous, orange-peel-textured bumps on the skin seen in graves’ disease.', 
    false)

WebUI.verifyTextPresent('Stare, lid lag.', false)

WebUI.verifyTextPresent('Graves’ ophthalmopathy - Protrusion of the eyeballs beyond their normal protective orbits, red swollen eyes, excessive tearing in one or both eyes, light sensitivity, blurry or double vision.', 
    false)

WebUI.verifyTextPresent('Irregular heartbeat, rapid heartbeat, palpitations (pounding of the heart).', false)

WebUI.verifyTextPresent('Frequent bowel movements, vomiting, abdominal pain, difficulty swallowing because of the enlarged thyroid gland.', 
    false)

WebUI.verifyTextPresent('Anxiety, restlessness, irritability, emotional lability, insomnia.', false)

WebUI.verifyTextPresent('Unintentional weight loss, increased appetite.', false)

WebUI.verifyTextPresent('Changes in menstrual patterns.', false)

WebUI.verifyTextPresent('Causes of Hyperthyroidism', false)

WebUI.verifyTextPresent('Graves’ disease (Body’s own immune system produces antibodies that stimulate the thyroid gland and produces thyroid hormone).', 
    false)

WebUI.verifyTextPresent('Hashitoxicosis (Body’s own immune system produces antibodies that stimulate the thyroid gland and produces thyroid hormone followed by no synthesis of thyroid hormone because of destruction of thyroid tissue).', 
    false)

WebUI.verifyTextPresent('Toxic adenoma (thyroid nodule that causes hyperthyroidism).', false)

WebUI.verifyTextPresent('Toxic multinodular goiter (multiple thyroid nodules that cause hyperthyroidism).', false)

WebUI.verifyTextPresent('Iodine - induced hyperthyroidism (excess iodine after procedures that use contrast agents, use of amiodarone).', 
    false)

WebUI.verifyTextPresent('Trophoblastic disease and germ cell tumors (Tumors that stimulate production of thyroid hormone).', 
    false)

WebUI.verifyTextPresent('Epoprostenol - medication that causes hyperthyroidism.', false)

WebUI.verifyTextPresent('Thyroiditis - Inflammation of the thyroid gland because of medications such as amiodarone, sunitinib, pazopanib, axitinib, interferon alfa, lithium; radiation.', 
    false)

WebUI.verifyTextPresent('Excess thyroid hormone originating from outside the thyroid gland.', false)

WebUI.verifyTextPresent('Complications of Hyperthyroidism', false)

WebUI.verifyTextPresent('Atrial fibrillation (a heart rhythm disorder), stroke, congestive heart failure (a condition in which your heart can\'t pump enough blood to meet your body\'s needs).', 
    false)

WebUI.verifyTextPresent('Impaired glucose tolerance in untreated patients.', false)

WebUI.verifyTextPresent('Exacerbation of the underlying asthma, respiratory muscle weakness.', false)

WebUI.verifyTextPresent('Malabsorption, cholestasis (Bile flow from the liver stops or slows).', false)

WebUI.verifyTextPresent('Anemia', false)

WebUI.verifyTextPresent('Vision loss in untreated hyperthyroidism.', false)

WebUI.verifyTextPresent('Untreated hyperthyroidism can lead to weak and brittle bones.', false)

WebUI.verifyTextPresent('Seek immediate medical help, if you experience any of these such as a sudden increase in the severity of your symptoms, fever, a rapid pulse and even delirium (confusion, reduced awareness of the surrounding).', 
    false)

WebUI.verifyTextPresent('Diagnosis of Hyperthyroidism', false)

WebUI.verifyTextPresent('Diagnosis of hyperthyroidism is done based on signs, symptoms, physical examination, and blood tests to measure thyroxine and TSH (thyroid-stimulating hormone) levels.', 
    false)

WebUI.verifyTextPresent('Additional tests are done to find out the cause of hyperthyroidism, if these blood tests are positive.', 
    false)

WebUI.verifyTextPresent('Additional tests may include radioiodine uptake test, thyroid scan, or thyroid ultrasound.', false)

WebUI.verifyTextPresent('Management of Hyperthyroidism', false)

WebUI.verifyTextPresent('Lifestyle management', false)

WebUI.verifyTextPresent('Follow a healthy diet and exercise regimen as recommended by your doctor. Exercise will help you feel better, improve muscle tone, helps maintain bone density.', 
    false)

WebUI.verifyTextPresent('Your doctor may recommend that you watch out for the amount of iodine in the diet. Seaweed contains a lot of iodine. Multivitamins, cough syrups may contain iodine.', 
    false)

WebUI.verifyTextPresent('Do not smoke, or quit smoking. Smoking can make your condition worse.', false)

WebUI.verifyTextPresent('Eye drops can be used to lubricate the eyes and relieve dryness. Wear sunglasses to protect the eyes.', 
    false)

WebUI.verifyTextPresent('Medical management', false)

WebUI.verifyTextPresent('Possible treatments may include:', false)

WebUI.verifyTextPresent('Radioactive iodine.', false)

WebUI.verifyTextPresent('Anti thyroid medications - propylthiouracil, or methimazole.', false)

WebUI.verifyTextPresent('Beta blockers - Propranolol, or atenolol.', false)

WebUI.verifyTextPresent('Surgery - surgery removes most of your thyroid gland.', false)

WebUI.verifyTextPresent('Prednisone to reduce the swelling behind the eyes in graves’ ophthalmopathy', false)

WebUI.verifyTextPresent('In some cases, surgical procedures may be done for graves’ ophthalmopathy.', false)

