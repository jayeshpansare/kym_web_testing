import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/febrile-seizures.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Febrile Seizures', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('FEBRILE SEIZURES', false)

WebUI.verifyTextPresent('Overview of Febrile Seizures', false)

WebUI.verifyTextPresent('Febrile seizure is a convulsion that usually occurs between six months and five years of age, associated with fever.', 
    false)

WebUI.verifyTextPresent('The degree of fever, not the rate of temperature rise, is the precipitating stimulus for febrile seizures.', 
    false)

WebUI.verifyTextPresent('Accepted criteria for Febrile Seizures include', false)

WebUI.verifyTextPresent('A seizure associated with an elevated temperature greater than 38°C.', false)

WebUI.verifyTextPresent('A child older than six months and younger than five years of age.', false)

WebUI.verifyTextPresent('No infection or inflammation in the central nervous system.', false)

WebUI.verifyTextPresent('Absence of metabolic abnormality that may produce convulsions.', false)

WebUI.verifyTextPresent('No history of previous afebrile seizures.', false)

WebUI.verifyTextPresent('Symptoms of Febrile Seizures', false)

WebUI.verifyTextPresent('Febrile seizures is characterized by shakes all over, and loss of consciousness.', false)

WebUI.verifyTextPresent('The child may get very stiff or twitch in just one area of the body.', false)

WebUI.verifyTextPresent('See your doctor soon after the febrile seizure or seek immediate medical help if the seizure lasts longer than five minutes or is associated with vomiting, stiff neck, extreme drowsiness, and breathing problems.', 
    false)

WebUI.verifyTextPresent('Types of Febrile Seizures', false)

WebUI.verifyTextPresent('Simple febrile seizures - Simple febrile seizures are generalized seizures that lasts less than 15 minutes and not recurring during a 24-hour period. The respiratory and facial muscles are most commonly involved. Simple febrile seizures are not specific to one part of the body.', 
    false)

WebUI.verifyTextPresent('Complex febrile seizures - Complex febrile seizure is confined to one side of the body, is recurrent within 24 hours, and lasts longer than 15 minutes.', 
    false)

WebUI.verifyTextPresent('Risk factors', false)

WebUI.verifyTextPresent('High fever.', false)

WebUI.verifyTextPresent('Viral infections are most commonly associated with febrile seizures.', false)

WebUI.verifyTextPresent('Post immunization with certain vaccines can increase the risk of febrile seizures. These include diphtheria, tetanus toxoid, and whole-cell pertussis (DTwP); and measles, mumps, and rubella (MMR). The fever, not the vaccination, causes the seizure.', 
    false)

WebUI.verifyTextPresent('Genetic predisposition.', false)

WebUI.verifyTextPresent('Other risk factors are prenatal exposure to nicotine, iron insufficiency.', false)

WebUI.verifyTextPresent('Diagnosis of Febrile Seizures', false)

WebUI.verifyTextPresent('Febrile seizure is a clinical diagnosis. The child\'s medical history and developmental history is carefully reviewed and other risk factors for epilepsy are excluded. The cause of the child’s fever is the first step after febrile seizure.', 
    false)

WebUI.verifyTextPresent('Tests that may be done are blood tests, urine test, spinal tap, electroencephalogram, magnetic resonance imaging.', 
    false)

WebUI.verifyTextPresent('Management of Rheumatoid Arthritis', false)

WebUI.verifyTextPresent('Febrile seizures can be managed better by taking certain preventive measures. Most febrile seizures occur in the first few hours of a fever. Giving the child acetaminophen, or ibuprofen at the beginning of a fever may make your child more comfortable, but it won\'t prevent a seizure. Avoid aspirin, as aspirin is linked to reye’s syndrome. Reye’s syndrome is a life threatening condition that can affect the blood, liver, and brain of someone who has recently had a viral infection. Aspirin is approved for use in children older than age 3, but children and teenagers recovering from chickenpox or flu-like symptoms should never take aspirin.', 
    false)

WebUI.verifyTextPresent('In children with a history of prolonged febrile seizure rectal diazepam (Diastat) or nasal midazolam might be prescribed.', 
    false)

WebUI.verifyTextPresent('These are steps to follow when your child has febrile seizures:', false)

WebUI.verifyTextPresent('Stay calm.', false)

WebUI.verifyTextPresent('Place your child on his or her side on a surface where he or she won\'t fall.', false)

WebUI.verifyTextPresent('Start timing the seizure.', false)

WebUI.verifyTextPresent('Stay close to watch and comfort your child.', false)

WebUI.verifyTextPresent('Do not restrain your child or interfere with your child\'s movements.', false)

WebUI.verifyTextPresent('Do not put anything in your child\'s mouth.', false)

WebUI.verifyTextPresent('Remove hard or sharp objects near your child.', false)

WebUI.verifyTextPresent('Loosen clothing if it is tight or restrictive.', false)

WebUI.verifyTextPresent('Seek immediate medical help if the seizure lasts longer than 5 minutes, or if your child has repeated seizures. Typically seizures stop on their own within a couple of minutes.', 
    false)

