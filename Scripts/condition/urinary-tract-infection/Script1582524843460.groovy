import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/urinary-tract-infection.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Urinary Tract Infection', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('URINARY TRACT INFECTION', false)

WebUI.verifyTextPresent('Overview of Urinary Tract Infection', false)

WebUI.verifyTextPresent('Urinary tract infection is an infection in any part of the urinary system - kidneys, ureters, bladder, and urethra which most commonly affects women.', 
    false)

WebUI.verifyTextPresent('Symptoms of Urinary Tract Infection', false)

WebUI.verifyTextPresent('Symptoms and signs depends on which part of the urinary tract is infected.', false)

WebUI.verifyTextPresent('Infection of kidneys - Pain in the side of the abdomen between hips and the ribs (flank); high fever with chills; nausea; vomiting.', 
    false)

WebUI.verifyTextPresent('Infection of bladder - Frequent, painful urination; pelvic pressure, sudden, compelling urge to urinate.', 
    false)

WebUI.verifyTextPresent('Infection of urethra - Burning with urination; discharge', false)

WebUI.verifyTextPresent('The infection could ascend via the urethra into the bladder and, to the kidneys via the ureters.', 
    false)

WebUI.verifyTextPresent('Causes of Urinary Tract Infection', false)

WebUI.verifyTextPresent('Escherichia coli is the most frequent bacterial cause of bladder infection. Other bacteria that occasionally cause bladder infection are Klebsiella pneumoniae, Proteus mirabilis, and Staphylococcus saprophyticus. All women are at risk of bladder infection because of their anatomy.', 
    false)

WebUI.verifyTextPresent('Infection of urethra could be from bacteria of the gastrointestinal tract. Also sexually transmitted pathogens can cause an infection of the urethra.', 
    false)

WebUI.verifyTextPresent('Complications of Urinary Tract Infection', false)

WebUI.verifyTextPresent('Kidney damage due to untreated urinary tract infection.', false)

WebUI.verifyTextPresent('Sepsis', false)

WebUI.verifyTextPresent('Urethral narrowing (stricture) in men from recurrent urethritis', false)

WebUI.verifyTextPresent('Seek immediate medical help, if you have persistently high fever with or without chills and pain.', 
    false)

WebUI.verifyTextPresent('Diagnosis of Urinary Tract Infection', false)

WebUI.verifyTextPresent('Urine sample analysis to look for white blood cells, red blood cells, bacteria.', false)

WebUI.verifyTextPresent('Urine culture - Urine culture is done to find out the bacteria causing urinary tract infection and which medications will be most effective.', 
    false)

WebUI.verifyTextPresent('Computerized tomography scan, or magnetic resonance imaging, or ultrasound - If you have frequent infections than your doctor may choose one of these options to look at your urinary tract for any abnormalities.', 
    false)

WebUI.verifyTextPresent('Cystoscope - This is done for recurrent urinary tract infections. A scope is inserted through the urethra and passed through to your bladder to see inside your urethra and bladder.', 
    false)

WebUI.verifyTextPresent('Management of Urinary Tract Infection', false)

WebUI.verifyTextPresent('Lifestyle management', false)

WebUI.verifyTextPresent('You can take steps to reduce the risk of urinary tract infection', false)

WebUI.verifyTextPresent('Drink plenty of water as advised by your doctor. Water helps dilute urine and you\'ll urinate more frequently flushing the bacteria out.', 
    false)

WebUI.verifyTextPresent('Drink a lot of cranberry juice. There is some indication that cranberry products may have infection-fighting properties.', 
    false)

WebUI.verifyTextPresent('Wiping from front to back after urinating and after a bowel movement prevents bacteria in the anal region from spreading to the vagina and urethra.', 
    false)

WebUI.verifyTextPresent('Avoid using feminine products in the genital area which can irritate the urethra.', false)

WebUI.verifyTextPresent('Contraceptive methods such as diaphragms, or unlubricated or spermicide-treated condoms should be changed as these can contribute to bacterial growth.', 
    false)

WebUI.verifyTextPresent('Postcoital voiding - Emptying the bladder soon after the intercourse will help flush bacteria out.', 
    false)

WebUI.verifyTextPresent('Medical management', false)

WebUI.verifyTextPresent('Uncomplicated urinary tract infection - Antibiotics that are commonly used are trimethoprim/sulfamethoxazole, fosfomycin, nitrofurantoin, cephalexin, ceftriaxone, ciprofloxacin, levofloxacin.', 
    false)

WebUI.verifyTextPresent('Complicated or severe urinary tract infection - Hospitalization and intravenous antibiotics are generally given.', 
    false)

