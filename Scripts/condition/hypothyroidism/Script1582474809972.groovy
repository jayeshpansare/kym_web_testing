import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/hypothyroidism.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Hypothyroidism', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('HYPOTHYROIDISM', false)

WebUI.verifyTextPresent('Overview of Hypothyroidism', false)

WebUI.verifyTextPresent('Hypothyroidism is a condition where the thyroid gland does not produce enough thyroid hormones to meet the body’s need.', 
    false)

WebUI.verifyTextPresent('Symptoms and signs of Hypothyroidism', false)

WebUI.verifyTextPresent('Symptoms - fatigue, weakness, weight gain, cold intolerance, constipation, growth failure, mental retardation (infantile onset), shortness of breath on exertion, swelling, dry skin, abnormally heavy bleeding at menstruation, decreased hearing, joint pain, muscle pain.', 
    false)

WebUI.verifyTextPresent('Signs - Coarse skin, enlargement of the tongue, swelling around the eyes, slow movement, slow speech, decrease in the heart rate, diastolic hypertension.', 
    false)

WebUI.verifyTextPresent('Causes of Hypothyroidism', false)

WebUI.verifyTextPresent('Chronic autoimmune (Hashimoto\'s) thyroiditis (Autoimmune disorder where antibodies are directed against the thyroid gland).', 
    false)

WebUI.verifyTextPresent('Thyroidectomy(surgical procedure where all or part of the thyroid gland is removed).', false)

WebUI.verifyTextPresent('Radioiodine treatment.', false)

WebUI.verifyTextPresent('External radiation therapy.', false)

WebUI.verifyTextPresent('Iodine deficiency.', false)

WebUI.verifyTextPresent('Iodine excess (Cough medicines, kelp tablets, iodine containing substances applied to the skin, amiodarone)', 
    false)

WebUI.verifyTextPresent('Medications such as lithium, ethionamide, amiodarone, interferon alfa, ipilimumab, pembrolizumab, nivolumab, sunitinib, sorafenib, imatinib, motesanib.', 
    false)

WebUI.verifyTextPresent('Diseases such as fibrous thyroiditis (Reidel\'s thyroiditis, a disorder where the fibrous tissue replaces the thyroid gland), hemochromatosis (a hereditary disorder where iron is deposited in the tissues), scleroderma (connective tissue disease that causes the skin to tighten, pain in the joints), leukemia (cancer of the body’s blood-forming tissues, and cystinosis (genetic, metabolic disorder), are rare causes of hypothyroidism.', 
    false)

WebUI.verifyTextPresent('Complications of Hypothyroidism', false)

WebUI.verifyTextPresent('Hypothyroid myopathy (Condition that affects the muscle and causes muscle weakness).', false)

WebUI.verifyTextPresent('Heart disease.', false)

WebUI.verifyTextPresent('Heart failure.', false)

WebUI.verifyTextPresent('Goiter (Constant stimulation of thyroid gland enlarges the gland - this condition is called goiter).', 
    false)

WebUI.verifyTextPresent('Peripheral neuropathy (Damage to the peripheral nerves which may cause pain, numbness and tingling).', 
    false)

WebUI.verifyTextPresent('Infertility.', false)

WebUI.verifyTextPresent('Depression and anxiety.', false)

WebUI.verifyTextPresent('Myxedema (Undiagnosed hypothyroidism can lead to a life threatening condition called myxedema characterized by intense cold intolerance, drowsiness, lethargy, unconsciousness).', 
    false)

WebUI.verifyTextPresent('Birth defects', false)

WebUI.verifyTextPresent('Diagnosis of Hypothyroidism', false)

WebUI.verifyTextPresent('The diagnosis of hypothyroidism is based on symptoms and signs, T3 and T4 (thyroid hormones) blood levels, and TSH(thyroid stimulating hormone) blood levels.', 
    false)

WebUI.verifyTextPresent('Management of Hypothyroidism', false)

WebUI.verifyTextPresent('Pharmacologic management', false)

WebUI.verifyTextPresent('Synthetic thyroxine - Levothyroxine.', false)

