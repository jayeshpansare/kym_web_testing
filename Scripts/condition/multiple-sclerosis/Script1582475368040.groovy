import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/multiple-sclerosis.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Multiple Sclerosis', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('MULTIPLE SCLEROSIS (MS)', false)

WebUI.verifyTextPresent('Overview of Multiple Sclerosis', false)

WebUI.verifyTextPresent('Multiple sclerosis is a disease of the central nervous system in which the immune system of the body attacks the protective sheath (myelin) that covers the nerve fibers. As the disease progresses there is permanent damage to the nerves. The cause of multiple sclerosis is unknown. A combination of genetics and environmental factors play a role in causing multiple sclerosis.', 
    false)

WebUI.verifyTextPresent('Symptoms of Multiple Sclerosis', false)

WebUI.verifyTextPresent('The signs and symptoms of multiple sclerosis may vary depending on the location of the affected nerve fibers, the amount of nerve damage.', 
    false)

WebUI.verifyTextPresent('Numbness or weakness that typically occurs in the legs and trunk, or one or more limbs on one side of your body at a time.', 
    false)

WebUI.verifyTextPresent('Lhermitte sign - Electric shock-like sensations that run down the back and/or limbs upon bending of the neck forward.', 
    false)

WebUI.verifyTextPresent('Gait disturbances and balance problems.', false)

WebUI.verifyTextPresent('Vision loss in one eye, double vision, blurry vision.', false)

WebUI.verifyTextPresent('Slurred speech; tingling and pain in parts of your body; Problems with sexual, bladder and bowel function; dizziness; weakness.', 
    false)

WebUI.verifyTextPresent('Pattern and course of Multiple Sclerosis', false)

WebUI.verifyTextPresent('Clinically isolated syndrome - First attack of multiple sclerosis. The symptoms and signs usually develop over the course of hours to days and then gradually remit over the ensuing weeks to months. Remission may not be complete.', 
    false)

WebUI.verifyTextPresent('Relapsing- remitting Multiple Sclerosis - These are clearly defined attacks with full or incomplete recovery. The symptoms and signs usually reach a peak in days to weeks, followed by a remission. The symptoms and signs resolve to a variable extent.', 
    false)

WebUI.verifyTextPresent('Secondary progressive Multiple Sclerosis - There is initial relapsing-remitting MS which gradually becomes worse with or without occasional relapses, minor remissions. This usually occurs 10 to 20 years after disease onset.', 
    false)

WebUI.verifyTextPresent('Primary progressive Multiple Sclerosis - There is progressive accumulation of disability from disease onset with temporary minor improvements.', 
    false)

WebUI.verifyTextPresent('Risk factors', false)

WebUI.verifyTextPresent('Family history - If one of your parents or siblings have a history of multiple sclerosis, you are at higher risk of developing the disease.', 
    false)

WebUI.verifyTextPresent('Age - Although multiple sclerosis can occur at any age, it usually affects people somewhere between the ages of 16 and 55.', 
    false)

WebUI.verifyTextPresent('Environmental factors', false)

WebUI.verifyTextPresent('Geographic latitude and place of birth. High frequency areas include all of europe (including Asian Russia), southern Canada, northern United States, New Zealand, and southeast Australia).', 
    false)

WebUI.verifyTextPresent('Having low levels of vitamin D and low exposure to sunlight is associated with a greater risk of multiple sclerosis.', 
    false)

WebUI.verifyTextPresent('Viral infections, particularly the Epstein-Barr virus, have been associated with multiple sclerosis.', 
    false)

WebUI.verifyTextPresent('Childhood or adolescent obesity and tobacco smoking may be risk factors for MS.', false)

WebUI.verifyTextPresent('Complications of Multiple Sclerosis', false)

WebUI.verifyTextPresent('Epilepsy', false)

WebUI.verifyTextPresent('Depression', false)

WebUI.verifyTextPresent('Paralysis, typically in the legs', false)

WebUI.verifyTextPresent('Mental changes, such as mood swings or forgetfulness.', false)

WebUI.verifyTextPresent('Problems with bladder, and bowel.', false)

WebUI.verifyTextPresent('Sexual dysfunction.', false)

WebUI.verifyTextPresent('Diagnosis of Multiple Sclerosis', false)

WebUI.verifyTextPresent('Diagnosis of multiple sclerosis is done with signs, symptoms, physical examination, and ruling out other conditions that might have similar signs and symptoms. Your doctor may recommend blood tests to check for specific biomarkers associated with MS.', 
    false)

WebUI.verifyTextPresent('Spinal tap (Sample of fluid removed from your spinal canal) to rule out infections and other conditions that may have similar symptoms. Magnetic resonance imaging may reveal lesions on your brain and spinal cord. Evoked potential tests - These tests record the electrical signals produced by your nervous system in response to stimuli.', 
    false)

WebUI.verifyTextPresent('Management of Multiple Sclerosis', false)

WebUI.verifyTextPresent('Lifestyle management', false)

WebUI.verifyTextPresent('Follow a diet and exercise regimen as recommended by your doctor. Talk to your doctor about taking vitamin D. Vitamin D may have potential benefit for people with MS. Exercise improves muscle tone, balance and coordination in mild to moderate multiple sclerosis.', 
    false)

WebUI.verifyTextPresent('Your doctor may evaluate you for sleep disorders. Make sure you are getting adequate sleep.', false)

WebUI.verifyTextPresent('The symptoms of multiple sclerosis may worsen when the body temperature rises. Avoid exposure to heat. Using cooling devices such as vests, scarves may be helpful.', 
    false)

WebUI.verifyTextPresent('Relieve stress with yoga, tai chi, massage, or meditation.', false)

WebUI.verifyTextPresent('Physical therapy and occupational therapy is helpful in people with multiple sclerosis.', false)

WebUI.verifyTextPresent('Medical management', false)

WebUI.verifyTextPresent('Multiple sclerosis attacks', false)

WebUI.verifyTextPresent('Multiple sclerosis attacks are managed with oral prednisone and intravenous methylprednisolone, to reduce nerve inflammation.', 
    false)

WebUI.verifyTextPresent('Plasma exchange, also called plasmapheresis is done for severe symptoms, and in those who haven\'t responded to steroids. Plasmapheresis is a technique in which the liquid part of the blood is removed. The blood cells are then mixed with a protein and put back into your body.', 
    false)

WebUI.verifyTextPresent('Muscle relaxants - Baclofen, tizanidine may be helpful to relieve painful or uncontrollable muscle stiffness or spasms.', 
    false)

WebUI.verifyTextPresent('Other medications - Medications may be used to treat fatigue, depression, sexual dysfunction, insomnia, bladder or bowel control problems.', 
    false)

WebUI.verifyTextPresent('Disease-modifying treatment', false)

WebUI.verifyTextPresent('Ocrelizumab', false)

WebUI.verifyTextPresent('Beta interferons', false)

WebUI.verifyTextPresent('Glatiramer acetate', false)

WebUI.verifyTextPresent('Fingolimod', false)

WebUI.verifyTextPresent('Dimethyl fumarate', false)

WebUI.verifyTextPresent('Teriflunomide', false)

WebUI.verifyTextPresent('Siponimod', false)

WebUI.verifyTextPresent('Natalizumab', false)

WebUI.verifyTextPresent('Alemtuzumab', false)

WebUI.verifyTextPresent('Mitoxantrone', false)

