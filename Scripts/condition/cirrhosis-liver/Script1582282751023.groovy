import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/cirrhosis-liver.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Cirrhosis Liver', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('CIRRHOSIS OF LIVER', false)

WebUI.verifyTextPresent('Overview of Cirrhosis of the Liver', false)

WebUI.verifyTextPresent('Cirrhosis is a late stage of progressive fibrosis of the liver caused by many forms of liver diseases and conditions. Each time there is damage to the liver caused by disease or excessive alcohol consumption, scar tissue (fibrous tissue) forms to repair the damage. As cirrhosis progresses, more and more scar tissue forms, making it difficult for the liver to function. Compensated cirrhosis is a condition wherein the liver is scarred but is still able to perform most of its basic function. Whereas in decompensated liver cirrhosis, the liver is scarred to such an extent that it is difficult for the liver to function.', 
    false)

WebUI.verifyTextPresent('Symptoms and signs of Cirrhosis', false)

WebUI.verifyTextPresent('Compensated cirrhosis may be asymptomatic or may have non-specific symptoms such as weight loss, weakness, loss of appetite.', 
    false)

WebUI.verifyTextPresent('Decompensated cirrhosis may present with', false)

WebUI.verifyTextPresent('Jaundice (yellowish discoloration of the skin and whites of eyes)', false)

WebUI.verifyTextPresent('Itchy skin', false)

WebUI.verifyTextPresent('Vomiting blood', false)

WebUI.verifyTextPresent('Dark tarry stool', false)

WebUI.verifyTextPresent('Passing fresh blood through the anus, usually with stool', false)

WebUI.verifyTextPresent('Abdominal distension with fluid accumulation', false)

WebUI.verifyTextPresent('Confusion', false)

WebUI.verifyTextPresent('slurred speech', false)

WebUI.verifyTextPresent('Sleep disturbances', false)

WebUI.verifyTextPresent('Absence of menstruation or irregular menstrual bleeding in women', false)

WebUI.verifyTextPresent('Swelling of the lower legs', false)

WebUI.verifyTextPresent('Weight loss', false)

WebUI.verifyTextPresent('Diarrhea', false)

WebUI.verifyTextPresent('Loss of sexual drive, impotence, infertility, breast enlargement in men.', false)

WebUI.verifyTextPresent('Spider-like blood vessels on the skin', false)

WebUI.verifyTextPresent('Easy bruising or bleeding.', false)

WebUI.verifyTextPresent('Causes of Cirrhosis', false)

WebUI.verifyTextPresent('Hepatitis B, C infections.', false)

WebUI.verifyTextPresent('Alcoholic liver disease.', false)

WebUI.verifyTextPresent('Nonalcoholic fatty liver disease (a condition where too much fat is stored in the liver cells affecting people who drink little to no alcohol).', 
    false)

WebUI.verifyTextPresent('Hemochromatosis (a condition where excess iron is stored in the body organs especially the liver, heart and pancreas).', 
    false)

WebUI.verifyTextPresent('Autoimmune hepatitis (Inflammation that occurs because the liver cells are being attacked by the body\'s immune system).', 
    false)

WebUI.verifyTextPresent('Primary sclerosing cholangitis (Long term disease that slowly damages the bile ducts).', false)

WebUI.verifyTextPresent('Primary and secondary biliary cirrhosis (Liver disease in which there is progressive destruction of the bile ducts).', 
    false)

WebUI.verifyTextPresent('Medications (example - isoniazid, methotrexate).', false)

WebUI.verifyTextPresent('Alpha-1 antitrypsin deficiency (inherited condition that raises the risk of liver and lung disease).', 
    false)

WebUI.verifyTextPresent('Infection (eg, brucellosis, syphilis, echinococcosis).', false)

WebUI.verifyTextPresent('Wilson disease (inherited condition in which copper builds up in the body.', false)

WebUI.verifyTextPresent('Idiopathic adulthood ductopenia.', false)

WebUI.verifyTextPresent('Celiac disease (autoimmune disease where the ingestion of gluten leads to damage in the small intestine).', 
    false)

WebUI.verifyTextPresent('Granulomatous liver disease (focal accumulations of inflammatory cells, commonly found in the liver).', 
    false)

WebUI.verifyTextPresent('Hereditary hemorrhagic telangiectasia (inherited disorder that causes abnormal connections between arteries and veins).', 
    false)

WebUI.verifyTextPresent('Idiopathic portal fibrosis.', false)

WebUI.verifyTextPresent('Polycystic liver disease (inherited condition in which fluid filled sacs develop in the liver).', 
    false)

WebUI.verifyTextPresent('Right-sided heart failure.', false)

WebUI.verifyTextPresent('Veno-occlusive disease (A condition in which small veins of the liver are obstructed).', false)

WebUI.verifyTextPresent('Complications of Cirrhosis', false)

WebUI.verifyTextPresent('Liver cancer.', false)

WebUI.verifyTextPresent('Ascitis (abdominal swelling due to fluid accumulation)', false)

WebUI.verifyTextPresent('Variceal bleeding (Bleeding of the abnormal enlarged veins in the esophagus).', false)

WebUI.verifyTextPresent('Spontaneous bacterial peritonitis (bacterial infection of the fluid that is accumulated in the abdomen).', 
    false)

WebUI.verifyTextPresent('Hepatic encephalopathy (There is a buildup of toxins in your bloodstream because of liver damage, which can lead to brain damage).', 
    false)

WebUI.verifyTextPresent('Portopulmonary hypertension (Hypertension of the pulmonary artery, blood vessel that supplies blood to lung and hypertension of portal vein, blood vessel that carries blood from the stomach, gallbladder, pancreas and spleen to the liver).', 
    false)

WebUI.verifyTextPresent('Portal vein thrombosis (blood clot that narrows or blocks portal vein).', false)

WebUI.verifyTextPresent('Diagnosis of Cirrhosis', false)

WebUI.verifyTextPresent('In people with symptoms and signs suggestive of cirrhosis magnetic resonance elastography is recommended.', 
    false)

WebUI.verifyTextPresent('Other imaging tests such as magnetic resonance imaging, computerized tomography, ultrasound, may also be done.', 
    false)

WebUI.verifyTextPresent('Blood tests are done to check bilirubin levels, liver enzymes, creatinine levels.', false)

WebUI.verifyTextPresent('Biopsy (tissue sample) may be done to identify the severity, extent and cause of liver damage.', 
    false)

WebUI.verifyTextPresent('Management of Cirrhosis', false)

WebUI.verifyTextPresent('Specific therapies are directed against the underlying cause of the cirrhosis. Complete abstinence from drinking alcohol substantially improves the survival in alcoholic liver cirrhosis. People with hepatitis C and cirrhosis should receive antiviral treatment.', 
    false)

WebUI.verifyTextPresent('Vaccination against hepatitis A and B for those who are not already immune to prevent further damage to the liver.', 
    false)

WebUI.verifyTextPresent('Vaccination against flu, pneumonia may also be recommended by your doctor.', false)

WebUI.verifyTextPresent('Avoidance of medications, supplements, and other substances that are commonly associated with liver injury. These medications includes abused substances, over the counter medications, herbal supplements. Talk to your doctor about the medications that you need to avoid.', 
    false)

WebUI.verifyTextPresent('Follow the diet recommended by your doctor. Reduce the amount of fatty and fried foods. Include whole grains, lean sources of protein, fruits, and vegetables. Low sodium diet may be recommended by your doctor to prevent fluid buildup in the body.', 
    false)

WebUI.verifyTextPresent('Medications to relieve certain symptoms, such as fatigue, itching, and pain.', false)

WebUI.verifyTextPresent('Nutritional supplements to counter malnutrition associated with cirrhosis and to prevent weak bones.', 
    false)

WebUI.verifyTextPresent('Beta blockers or endoscopic variceal(enlarged veins) ligation for prevention of variceal bleeding.', 
    false)

WebUI.verifyTextPresent('Antibiotics for spontaneous bacterial peritonitis.', false)

WebUI.verifyTextPresent('For decompensated cirrhosis, liver transplantation is definitive treatment.', false)

WebUI.verifyTextPresent('Patients with cirrhosis should follow up with ultrasonography every six months to look for signs of liver cancer.', 
    false)

WebUI.verifyTextPresent('Your doctor will recommend an upper endoscopy at regular intervals to look for enlarged veins in the esophagus or stomach (varices) that may bleed.', 
    false)

