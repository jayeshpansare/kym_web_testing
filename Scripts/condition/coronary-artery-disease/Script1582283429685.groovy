import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/coronary-artery-disease.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Coronary Artery Diseases', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('CORONARY ARTERY DISEASE/ISCHEMIC HEART DISEASE', false)

WebUI.verifyTextPresent('Overview of Coronary Artery Disease', false)

WebUI.verifyTextPresent('Coronary artery disease happens due to the buildup of cholesterol and other material, called plaque, on the inner walls of the arteries that supply blood to the heart. This buildup is called atherosclerosis. Arteries become hard and narrow. The heart muscle can\'t get the blood or oxygen it needs.', 
    false)

WebUI.verifyTextPresent('Symptoms of Coronary Artery Disease', false)

WebUI.verifyTextPresent('Chest pain also referred to as angina - Tightness or pressure on the middle or left side of the chest. In some people, especially the women, pain may be sharp or fleeting and felt in the neck, arm or back. Pain is triggered by physical or emotional stress and often goes away within minutes after stopping the stressful activity.', 
    false)

WebUI.verifyTextPresent('Shortness of breath.', false)

WebUI.verifyTextPresent('Fatigue with exertion.', false)

WebUI.verifyTextPresent('Heart attack - This occurs if the coronary artery is completely blocked. You may feel crushing pressure in your chest and pain in your shoulder or arm sometimes associated with shortness of breath and sweating. Some people may not experience neck and jaw pain, especially women.', 
    false)

WebUI.verifyTextPresent('If you experience symptoms of a heart attack, seek immediate medical help.', false)

WebUI.verifyTextPresent('Risk factors', false)

WebUI.verifyTextPresent('Smoking,', false)

WebUI.verifyTextPresent('High blood pressure,', false)

WebUI.verifyTextPresent('High cholesterol levels (high levels of low-density lipoproteins, also called bad cholesterol and low levels of high-density lipoproteins, also called good cholesterol),', 
    false)

WebUI.verifyTextPresent('Diabetes,', false)

WebUI.verifyTextPresent('Overweight, obesity,', false)

WebUI.verifyTextPresent('Physical inactivity,', false)

WebUI.verifyTextPresent('Stress,', false)

WebUI.verifyTextPresent('Unhealthy diet,', false)

WebUI.verifyTextPresent('Old age.', false)

WebUI.verifyTextPresent('Diagnosis of Coronary Artery Disease', false)

WebUI.verifyTextPresent('The diagnosis of coronary artery disease is made with classic symptoms of chest pain and electrocardiogram. In a stable patient, the diagnosis is confirmed with the stress test. The choice of stress test can be stress echocardiography or stress nuclear imaging, or coronary cardiac computed angiography.', 
    false)

WebUI.verifyTextPresent('Patients presenting with symptoms of acute coronary syndrome or unstable patients - repeated electrocardiogram and serial high sensitivity troponins levels are obtained.', 
    false)

WebUI.verifyTextPresent('Complications and progression of Coronary Artery Disease', false)

WebUI.verifyTextPresent('Stable ischemic heart disease can progress to unstable ischemic heart disease [unstable angina, myocardial infarction (heart attack)].', 
    false)

WebUI.verifyTextPresent('Heart failure.', false)

WebUI.verifyTextPresent('Arrhythmia (abnormal heart rhythm).', false)

WebUI.verifyTextPresent('Management of Coronary Artery Disease', false)

WebUI.verifyTextPresent('Lifestyle management', false)

WebUI.verifyTextPresent('Diet recommended for coronary artery disease - People with coronary artery disease have to learn to balance the meals and make the healthiest food choices. Consume the number of calories recommended by your doctor or dietician.', 
    false)

WebUI.verifyTextPresent('Trans fat (hydrogenated fats) are atherogenic, while mono- and polyunsaturated fats (particularly omega-3 fatty acids) - eg, those found in fish, olive oil, nuts are protective and prevent and treat cardiovascular disease.', 
    false)

WebUI.verifyTextPresent('The usual daily intake of protein should be approximately 10 to 20 percent of total caloric intake. Higher levels of dietary protein intake (>20 percent of calories from protein or >1.3 g/kg/day) have been associated with increased albuminuria, more rapid kidney function loss, and cardiovascular disease (CVD) mortality and therefore should be avoided.', 
    false)

WebUI.verifyTextPresent('Reduced sodium intake of 2300 mg per day. Further reduction is needed if you have hypertension.', 
    false)

WebUI.verifyTextPresent('Note - Diet recommendations for refractory heart failure which could be a complication of coronary artery disease, may vary from these recommendations.', 
    false)

WebUI.verifyTextPresent('Physical activity - Before starting any exercise regimen talk to your doctor. Your doctor may do a physical examination and tests to see if you are fit for starting an exercise regimen. After advice from your doctor, the recommended physical activity for stable coronary artery disease is at least ≥30 minutes per day of moderate activity.', 
    false)

WebUI.verifyTextPresent('Stress - Stress should be reduced and talk to your doctor about the management of underlying depression and anxiety if you have these associated conditions.', 
    false)

WebUI.verifyTextPresent('Medical management of stable ischemic heart disease', false)

WebUI.verifyTextPresent('Beta blockers - propranolol, atenolol, metoprolol, nadolol.', false)

WebUI.verifyTextPresent('Calcium channel blockers - Amlodipine, felodipine, verapamil, diltiazem.', false)

WebUI.verifyTextPresent('Long-acting nitrates - Isosorbide mononitrate, isosorbide dinitrate.', false)

WebUI.verifyTextPresent('Ranolazine.', false)

WebUI.verifyTextPresent('Sublingual nitroglycerin for acute treatment of chest pain.', false)

WebUI.verifyTextPresent('To prevent disease progression - Aspirin, statins (cholesterol-lowering medication).', false)

WebUI.verifyTextPresent('Angiotensin-converting enzyme inhibitors or angiotensin receptor blockers (ARBs) in a subset of patients with stable ischemic heart disease, such as those with hypertension, diabetes mellitus.', 
    false)

WebUI.verifyTextPresent('Angiography and revascularization - In patients with stable ischemic heart disease there are two indications for angiography and revascularization.', 
    false)

WebUI.verifyTextPresent('Angina that significantly interferes with a patient\'s daily activities despite maximal tolerable medical therapy.', 
    false)

WebUI.verifyTextPresent('The noninvasive testing indicating a high likelihood of severe ischemic heart disease.', false)

