import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def getGlobalURL = GlobalVariable.Site_URL

def pageURL = getGlobalURL + '/disease-and-vitals/heart-failure.html'

WebUI.navigateToUrl(pageURL)

WebUI.waitForPageLoad(10)

not_run: WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

pageTitle = WebUI.getWindowTitle()

WebUI.verifyMatch(pageTitle, 'Heart Failure', false)

getCurrentPageURL = WebUI.getUrl()

WebUI.verifyMatch(getCurrentPageURL, pageURL, false)

WebUI.verifyTextPresent('HEART FAILURE', false)

WebUI.verifyTextPresent('Overview of Heart Failure', false)

WebUI.verifyTextPresent('Heart failure is a syndrome that has resulted from structural or functional heart disorder. The ability of the heart to fill or eject blood is impaired.', 
    false)

WebUI.verifyTextPresent('There are two types of heart failure:', false)

WebUI.verifyTextPresent('Heart failure with reduced left ventricular ejection fraction, also called systolic heart failure.', 
    false)

WebUI.verifyTextPresent('Heart failure with preserved ejection fraction, also called diastolic heart failure.', false)

WebUI.verifyTextPresent('Classification of Heart Failure', false)

WebUI.verifyTextPresent('ACC/AHA stages of heart failure are:', false)

WebUI.verifyTextPresent('Stage A - Patients at risk for developing heart failure. Includes patients with diabetes mellitus, hypertension, atherosclerotic disease, metabolic syndrome, obesity, or patients with a family history of cardiomyopathy.', 
    false)

WebUI.verifyTextPresent('Stage B - Patients with structural heart disease but without signs or symptoms of heart failure.', 
    false)

WebUI.verifyTextPresent('Stage C - Patients with structural heart disease, with previous or current symptoms of heart failure.', 
    false)

WebUI.verifyTextPresent('Stage D - Patients with advanced structural heart failure requiring specialized interventions.', 
    false)

WebUI.verifyTextPresent('Symptoms of Heart Failure', false)

WebUI.verifyTextPresent('Shortness of breath at rest or on exertion, reduced exercise tolerance, shortness of breath while lying flat, shortness of breath that occurs at night and awakens you from sleep, swelling.', 
    false)

WebUI.verifyTextPresent('Causes and risk factors of Heart Failure', false)

WebUI.verifyTextPresent('Coronary artery disease, hypertension, diabetes mellitus, obesity, valvular heart disease, cardiomyopathy (disease where the heart muscle is enlarged, thick or rigid).', 
    false)

WebUI.verifyTextPresent('Complications of Heart Failure', false)

WebUI.verifyTextPresent('Kidney damage or kidney failure, heart rhythm problems, heart valve problems, liver damage.', false)

WebUI.verifyTextPresent('Diagnosis of Heart Failure', false)

WebUI.verifyTextPresent('Diagnosis of heart failure is based on clinical features, physical examination, may include tests such as blood test, echocardiography, echocardiogram, chest x ray, stress test, computerized tomography scan, magnetic resonance imaging, coronary angiogram.', 
    false)

WebUI.verifyTextPresent('Management of Heart Failure', false)

WebUI.verifyTextPresent('Lifestyle management', false)

WebUI.verifyTextPresent('Diet - Follow the diet recommended by your doctor and diet for the underlying diseases. Consume the number of calories recommended by your doctor or dietician.', 
    false)

WebUI.verifyTextPresent('Trans fat (hydrogenated fats) are atherogenic, while mono- and polyunsaturated fats (particularly omega-3 fatty acids) - eg, those found in fish, olive oil, nuts are protective and prevent and treat cardiovascular disease.', 
    false)

WebUI.verifyTextPresent('Fiber intake should be at least 14 grams per 1000 calories daily.', false)

WebUI.verifyTextPresent('Sodium(salt) restriction - Patients with symptomatic heart failure should restrict their sodium(salt) intake to less than 3 grams a day.', 
    false)

WebUI.verifyTextPresent('Weight - Weight needs to be checked everyday. A reasonable plan might be that for a two- to five-pound weight gain in one week. Weight gain more than 5 pounds in one week requires immediate call to your doctor or nurse.', 
    false)

WebUI.verifyTextPresent('Swelling - Check your legs each day for swelling. Progressive and severe swelling requires immediate call to your doctor or nurse.', 
    false)

WebUI.verifyTextPresent('Exercise tolerance - Use a scale ranging from no shortness of breath, shortness of breath after moderate exertion, shortness of breath after mild exertion, shortness of breath at rest. Severe shortness of breath requires immediate call to your doctor or nurse.', 
    false)

WebUI.verifyTextPresent('Breathing at night - Use a scale ranging from no shortness of breath lying flat, needing two pillows or more, sleeping upright or awakening with sudden shortness of breath. Severe shortness of breath requires immediate call to your doctor or nurse.', 
    false)

WebUI.verifyTextPresent('Dizziness and lightheadedness - Use a scale ranging from not dizzy, dizzy for a while after standing, near syncope/syncope or fall. Severe dizziness and syncope requires immediate call to your doctor or nurse.', 
    false)

WebUI.verifyTextPresent('Fluid restriction - Fluid should be restricted to 1.5 - 2 litres a day in patients with refractory heart failure.', 
    false)

WebUI.verifyTextPresent('Pharmacologic management', false)

WebUI.verifyTextPresent('The medications used for heart failure are:', false)

WebUI.verifyTextPresent('Angiotensin converting enzyme inhibitors - lisinopril, enalapril, captopril.', false)

WebUI.verifyTextPresent('Angiotensin II receptor blockers - losartan, valsartan, candesartan', false)

WebUI.verifyTextPresent('Beta blockers - Carvedilol, bisoprolol, metoprolol', false)

WebUI.verifyTextPresent('Hydralazine and nitrates', false)

WebUI.verifyTextPresent('Diuretics - Furosemide, bumetanide, torsemide, metolazone,Spironolactone,Digoxin and warfarin/dabigatran.', 
    false)

