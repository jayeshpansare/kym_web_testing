<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Web_test_Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b6c57f9d-1a54-486b-9174-cf0d715d75ca</testSuiteGuid>
   <testCaseLink>
      <guid>325b7db2-9283-4cf4-84cc-9fb7ab53ef9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/openSite</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0189b1db-1319-43cc-8f02-1e3561c4bf88</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/arterial-blood-gases-test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ec2a1713-6b06-42a0-abf4-d5dfb555e19d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/blood-albumin-levels</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d331fe85-6e39-4f0d-8ed1-c2bb3aac84b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/blood-antiseizure-drug-levels</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bfd47425-bf7a-4ecc-9075-4dbe6772ebe5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/blood-cholesterol-levels</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf872d00-dfde-4f8a-8fa1-303c7c52a39e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/blood-creatinine-levels</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>54273972-35d7-46fe-8cef-19ca62af0a8c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/blood-globulin-level</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>69d5cfe9-5ad8-4b28-97f4-3fcd5d41967f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/blood-glucose-levels</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1653605-d088-484d-892c-a03d52b8e78f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/blood-pressure</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4dec2654-bed3-482a-a28c-f72a3a98a8f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/blood-sodium-levels</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b7686c8-e553-4402-a450-0f43a33a6b0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/blood-test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f9d8f9c4-b1fe-4925-8e92-44d67d608475</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/blood-vitamin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5f50d421-d0d4-4b24-aeb6-73dd2f7ebe6b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/bone-density-test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bdaf90a4-56f8-46e6-9b67-825682ce7943</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/complete-blood-count</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>533a6d17-8cdd-4fbd-9181-2c34b559aeaa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/ct-scan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cea270f1-fa2a-4021-a806-3d9ee0fd0feb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/dentist-appointment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26e0864b-1cb9-4b17-8476-77d89b72ab23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/doctor-appointment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>517b960c-ec35-48bf-85f0-e3bff83183c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/electrocardiogram-Test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1233f96d-5a63-4755-a60a-3eae67a1737b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/electrolyte-panel</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9310fa5e-4de7-4948-b6c6-3b7a9e698686</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/endometrial-biopsy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d3b2ff5-deed-4d8d-84b5-6d6d4ba003bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/eye-examination</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c2070b7b-7dd4-446a-aa88-773084bdbf46</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/fasting-glucose-levels</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45689883-302e-43ac-9f32-f8e915db7065</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/generalized-anxiety-disorder</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>16617e95-9b19-488c-be84-956d328f95f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/heart-rate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>64cfbce3-87f7-4afe-a9d7-d8b8833ccd2c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/hemoglobin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a4b4b1a-a80c-4b8b-a090-b6bc7a5603bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/inr-test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa659f37-81f2-4c3f-8bbd-91b7504494a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/kidney-function-test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d169b52f-c6d4-4b98-a207-cc2acb95a018</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/liver-function-test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae166d7a-37dc-4b4e-b1e0-3af008c8a2c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/low-dose-computed-tomography</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a48447ae-da46-4c52-82da-17b8bec8bcd8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/mri-scan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8c5b2c9-f145-4235-9257-22baea113d7e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/neurocognitive-testing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a4fec65-7f84-4d47-9dc1-2aaff379f147</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/neuropsychological-tests</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>43eba35d-2651-40a3-ad12-93bc8bed1d4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/oral-glucose-tolerance-test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a4c460fb-3e88-4327-a8f5-b813690860ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/proximal-compressive-ultrasound</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab8daf81-6c84-462a-88ae-4b0f28fabde9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/pulmonary-function-test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>89cb820b-cbcf-4caa-aeef-f125e69f1781</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/pulse-oximetry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d452ddc3-8897-4e62-a832-1d01deb638e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/respiratory-rate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4764fa53-5901-4198-8c95-17ad33b0ee4d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/serum-creatinine-level</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45fdc725-204b-4853-964a-68fd722db19e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/serum-urate-levels</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58945637-addf-4fb7-ba8f-dfa2a664361f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/temperature</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb6de9e8-2c7f-404f-8d8d-42d5a2a5178d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/thyroid-function-test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7e4bc03f-4fa9-4296-aa1a-c3e8549d2226</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/thyrotropin-receptor-antibodies</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41edb429-eb0a-421a-a58a-34681cd179a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/transcranial-doppler-ultrasound</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2167fa0-fa2c-4de3-9ed4-8edfb869baf3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/transthoracic-doppler-echocardiogram</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>95008272-1fde-47a1-a07a-1788fd88c036</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/ultrasonography</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>abc39ccf-17d1-4837-aba2-3cc71205ce6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/upper-endoscopy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dc3c8006-3bb9-411c-a696-c889f2b2dc09</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/urine-albumin-to-creatinine-ratio</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41201ee9-6f79-46b5-a14e-172fbd581eca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/urine-analysis</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4cc7aad0-6e94-4a2a-bd40-11b6289ae8ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/Weight</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5b96635-16e4-49f3-830d-08ffe760508b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/abdominal-aortic-aneurysm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>efee3790-4aa1-44e7-b84e-438fe78c7eba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/asthma</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c4efc04-afe2-47c9-9977-d375bcb18750</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/chronic-obstructive-pulmonary-disease</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f9eb2745-b0ed-4189-8ff9-bc5b40b331ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/cirrhosis-liver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c6b5a2b-fa60-4a15-ac5c-1c60574c7175</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/coronary-artery-disease</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>97cd5c7a-4438-4258-a40d-a502f842cb5a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/deep-vein-thrombosis</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04e900c7-6fa2-4520-a1dd-ad31d8f5f0c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/diabetes-mellitus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>84d327d1-7945-4bf1-bfeb-b97a8bf55b7f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/diabetes-mellitus-type1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f30e27a4-493d-466f-b758-9a567cf97dea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/epilepsy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a76afe82-2c35-4330-8dd5-0a893a765273</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/febrile-seizures</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b9f873e-113e-44af-9b47-d31f10d02ea2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/gastroesophageal-reflux-disease</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1bf457c1-996a-410a-a207-bd65702d6baa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/generalized-anxiety-disorder</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8375a923-37c2-43a7-91df-33574406e18b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/gout</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c9c0ed26-64e5-4c8b-9fa0-bd4c9172fb62</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/heart-failure</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>70a1040b-6ee6-46f1-8c73-a4d43ade5f2f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/hyperthyroidism</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d56d68d3-91bf-42a4-a224-141fb1cbcf8e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/hypothyroidism</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>03f08fb6-c599-4908-8d82-0cce147a3b08</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/multiple-sclerosis</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0740ad4-1bf0-41fc-baaf-d3ed347724ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/polycystic-ovary-syndrome</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40853846-adaa-4ed8-9f4a-6d1239c9cdab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/rheumatoid-arthritis</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a3e303ba-df2c-41f2-827c-0e6f93b9f466</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/sickle-cell-anemia</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>496d9781-fc27-4b1b-ad5f-910618b9ddf2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/stroke</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>61a6133d-1fb9-49f5-8ef6-7e72e3a0a720</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/condition/urinary-tract-infection</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c89ca54-2f7c-4fdc-ad53-fef1f4867e9d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Disease and Vital/x-ray</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59a8b4eb-4b9f-4ff4-ac6c-e2718d597d0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/closeSite</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
